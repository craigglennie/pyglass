"""initial

Revision ID: 6e3d17f05338
Revises: 
Create Date: 2017-08-27 11:51:43.921462

"""
from alembic import op
import re
import sqlalchemy as sa

# revision identifiers, used by Alembic.
revision = '6e3d17f05338'
down_revision = None
branch_labels = None
depends_on = None


def create_table(sql):
    assert 'CREATE TABLE' in sql
    table_name = re.match('\s*CREATE TABLE (\w*)\s', sql).group(1)
    op.execute(sql)
    op.execute(f"CREATE TRIGGER {table_name}_updated_at BEFORE UPDATE ON {table_name} FOR EACH ROW EXECUTE PROCEDURE update_updated_at_column()");


def upgrade():
    op.execute(
        """
        CREATE FUNCTION update_updated_at_column() RETURNS trigger
            LANGUAGE plpgsql
            AS $$
          BEGIN
            NEW.updated_at = TIMEZONE('utc', CURRENT_TIMESTAMP);
            RETURN NEW;
          END;
        $$;
        """
    )

    op.execute(
        """
        CREATE TYPE requirement_type AS ENUM ('run', 'test', 'build', 'dev');
        """
    )

    create_sql = [
        """
        CREATE TABLE queued_updates (
            id SERIAL NOT NULL, 
            created_at TIMESTAMP WITHOUT TIME ZONE DEFAULT TIMEZONE('utc', CURRENT_TIMESTAMP) NOT NULL, 
            updated_at TIMESTAMP WITHOUT TIME ZONE DEFAULT NULL,
            package_name VARCHAR(100), 
            back_off_factor INTEGER, 
            retry_after TIMESTAMP WITHOUT TIME ZONE, 
            PRIMARY KEY (id), 
            UNIQUE (package_name)
        );
        """,

        """
        CREATE TABLE packages (
            id SERIAL NOT NULL, 
            created_at TIMESTAMP WITHOUT TIME ZONE DEFAULT TIMEZONE('utc', CURRENT_TIMESTAMP) NOT NULL, 
            updated_at TIMESTAMP WITHOUT TIME ZONE DEFAULT NULL,
            name VARCHAR(100), 
            author VARCHAR, 
            home_page VARCHAR(200), 
            summary VARCHAR, 
            PRIMARY KEY (id), 
            UNIQUE (name)
        )
        """,

        """
        CREATE TABLE successful_bigquery_extractions (
            created_at TIMESTAMP WITHOUT TIME ZONE DEFAULT TIMEZONE('utc', CURRENT_TIMESTAMP) NOT NULL, 
            updated_at TIMESTAMP WITHOUT TIME ZONE DEFAULT NULL,
            table_name VARCHAR(17) NOT NULL, 
            completed_at TIMESTAMP WITHOUT TIME ZONE, 
            PRIMARY KEY (table_name)
        )     
        """,

        """
        CREATE TABLE releases (
            id SERIAL NOT NULL, 
            created_at TIMESTAMP WITHOUT TIME ZONE DEFAULT TIMEZONE('utc', CURRENT_TIMESTAMP) NOT NULL, 
            updated_at TIMESTAMP WITHOUT TIME ZONE DEFAULT NULL,
            package_id INTEGER NOT NULL, 
            released_at TIMESTAMP WITHOUT TIME ZONE, 
            version VARCHAR(100), 
            ordering INTEGER, 
            is_queued BOOLEAN NOT NULL DEFAULT FALSE,
            requirements_attempt_count INTEGER NOT NULL DEFAULT 0, 
            requirements_error_reason VARCHAR DEFAULT NULL,
            requirements_error_message VARCHAR DEFAULT NULL,
            PRIMARY KEY (id), 
            FOREIGN KEY(package_id) REFERENCES packages (id),
            UNIQUE (package_id, version)
        )
        """,

        """
        CREATE TABLE requirements (
            id SERIAL NOT NULL, 
            created_at TIMESTAMP WITHOUT TIME ZONE DEFAULT TIMEZONE('utc', CURRENT_TIMESTAMP) NOT NULL, 
            updated_at TIMESTAMP WITHOUT TIME ZONE DEFAULT NULL,
            release_id INTEGER NOT NULL, 
            package_id INTEGER NOT NULL, 
            specifier VARCHAR, 
            environment VARCHAR(50) DEFAULT NULL,
            from_wheel BOOLEAN NOT NULL,
            req_type requirement_type NOT NULL,
            PRIMARY KEY (id), 
            FOREIGN KEY(release_id) REFERENCES releases (id), 
            FOREIGN KEY(package_id) REFERENCES packages (id)
        )
        """,

        """
        CREATE TABLE unresolved_requirements (
            id SERIAL NOT NULL, 
            created_at TIMESTAMP WITHOUT TIME ZONE DEFAULT TIMEZONE('utc', CURRENT_TIMESTAMP) NOT NULL, 
            updated_at TIMESTAMP WITHOUT TIME ZONE DEFAULT NULL,
            release_id INTEGER NOT NULL, 
            queued_update_id INTEGER NOT NULL, 
            specifier VARCHAR, 
            from_wheel BOOLEAN NOT NULL,
            environment VARCHAR(50) DEFAULT NULL,
            req_type requirement_type NOT NULL,
            PRIMARY KEY (id), 
            FOREIGN KEY(release_id) REFERENCES releases (id), 
            FOREIGN KEY(queued_update_id) REFERENCES queued_updates (id)
        )       
        """,

        """
        CREATE TABLE downloads (
            id SERIAL NOT NULL, 
            created_at TIMESTAMP WITHOUT TIME ZONE DEFAULT TIMEZONE('utc', CURRENT_TIMESTAMP) NOT NULL, 
            updated_at TIMESTAMP WITHOUT TIME ZONE DEFAULT NULL,
            package_id INTEGER NOT NULL, 
            release_id INTEGER NOT NULL, 
            python_version VARCHAR(20), 
            for_date DATE NOT NULL, 
            downloads INTEGER NOT NULL, 
            PRIMARY KEY (id), 
            FOREIGN KEY(package_id) REFERENCES packages (id), 
            FOREIGN KEY(release_id) REFERENCES releases (id),
            UNIQUE (for_date, package_id, release_id, python_version)
        )  
        """,

        """
        CREATE TABLE staging_downloads (
            id SERIAL NOT NULL, 
            created_at TIMESTAMP WITHOUT TIME ZONE DEFAULT TIMEZONE('utc', CURRENT_TIMESTAMP) NOT NULL, 
            updated_at TIMESTAMP WITHOUT TIME ZONE DEFAULT NULL,
            package_name VARCHAR(100) NOT NULL, 
            package_id INTEGER, 
            package_version VARCHAR(100) NOT NULL, 
            release_id INTEGER, 
            python_version VARCHAR(20), 
            for_date DATE NOT NULL, 
            downloads INTEGER NOT NULL, 
            PRIMARY KEY (id), 
            FOREIGN KEY(package_id) REFERENCES packages (id), 
            FOREIGN KEY(release_id) REFERENCES releases (id)
        )
        """,

    ]

    for statement in create_sql:
        create_table(statement)

    op.execute("CREATE INDEX ix_releases_version ON releases (version)")
    op.execute("CREATE INDEX ix_downloads_python_version ON downloads (python_version)")
    op.execute("CREATE INDEX ix_staging_downloads_package_name ON staging_downloads (package_name)")
    op.execute("CREATE UNIQUE INDEX ix_requirements_unique ON requirements "
               "(release_id, package_id, req_type, COALESCE(environment, ''))")
    op.execute("CREATE UNIQUE INDEX ix_unresolved_unique ON unresolved_requirements "
               "(release_id, queued_update_id, req_type, COALESCE(environment, ''))")


def downgrade():
    sql = [
        "DROP TABLE IF EXISTS staging_downloads",
        "DROP TABLE IF EXISTS downloads",
        "DROP TABLE IF EXISTS unresolved_requirements",
        "DROP TABLE IF EXISTS requirements",
        "DROP TABLE IF EXISTS releases",
        "DROP TABLE IF EXISTS successful_bigquery_extractions",
        "DROP TABLE IF EXISTS packages",
        "DROP TABLE IF EXISTS queued_updates",
        "DROP FUNCTION IF EXISTS update_updated_at_column()",
        "DROP TYPE IF EXISTS requirement_type"
    ]
    for statement in sql:
        op.execute(statement);

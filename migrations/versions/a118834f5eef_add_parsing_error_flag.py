"""add parsing error flag

Revision ID: a118834f5eef
Revises: 2a2777d907c3
Create Date: 2017-10-29 10:04:52.082421

"""
from alembic import op
import sqlalchemy as sa
import re



# revision identifiers, used by Alembic.
revision = 'a118834f5eef'
down_revision = '2a2777d907c3'
branch_labels = None
depends_on = None


def upgrade():
    op.execute("ALTER TABLE releases ADD COLUMN "
               "error_parsing_requirements BOOLEAN NULL DEFAULT NULL")
    op.execute("UPDATE releases SET error_parsing_requirements = FALSE")


def downgrade():
    op.execute("ALTER TABLE releases DROP COLUMN error_parsing_requirements")

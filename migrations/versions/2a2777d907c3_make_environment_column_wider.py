"""make environment column wider

Revision ID: 2a2777d907c3
Revises: 6e3d17f05338
Create Date: 2017-10-27 06:37:30.313023

"""
from alembic import op
import sqlalchemy as sa
import re



# revision identifiers, used by Alembic.
revision = '2a2777d907c3'
down_revision = '6e3d17f05338'
branch_labels = None
depends_on = None


def upgrade():
    for table in ['requirements', 'unresolved_requirements']:
        op.execute(f"ALTER TABLE {table} ALTER COLUMN environment SET DATA TYPE VARCHAR")


def downgrade():
    for table in ['requirements', 'unresolved_requirements']:
        op.execute(f"ALTER TABLE {table} ALTER COLUMN environment SET DATA TYPE VARCHAR(50)")

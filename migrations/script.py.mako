"""${message}

Revision ID: ${up_revision}
Revises: ${down_revision | comma,n}
Create Date: ${create_date}

"""
from alembic import op
import sqlalchemy as sa
import re

${imports if imports else ""}

# revision identifiers, used by Alembic.
revision = ${repr(up_revision)}
down_revision = ${repr(down_revision)}
branch_labels = ${repr(branch_labels)}
depends_on = ${repr(depends_on)}


def create_table(sql):
    assert 'CREATE TABLE' in sql
    table_name = re.match('\s*CREATE TABLE (\w*)\s', sql).group(1)
    op.execute(sql)
    op.execute(f"CREATE TRIGGER {table_name}_updated_at BEFORE UPDATE ON {table_name} FOR EACH ROW EXECUTE PROCEDURE update_updated_at_column()");


def upgrade():
    ${upgrades if upgrades else "pass"}


def downgrade():
    ${downgrades if downgrades else "pass"}

from importlib import reload
from pyglass.models import *
from pyglass.db import get_or_create_session
session = get_or_create_session()


def show_running_queries():
    result = session.execute("""
    SELECT pid, age(query_start, clock_timestamp()), usename, query
    FROM pg_stat_activity
    WHERE query != '<IDLE>' AND query NOT ILIKE '%pg_stat_activity%'
    ORDER BY query_start desc;
    """)
    return result.fetchall()


print("\nWelcome to PyGlass shell. Database models imported. 'session' created.")

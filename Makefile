SHELL = /bin/bash
ROOT = $(shell pwd)
.PHONY: start-test-db, start-dev-db, s3-copy-files, dev-web, dev-shell, test-shell


run-test-db:
	@docker run --rm --name test-postgres -e POSTGRES_PASSWORD=thisisatest -e PGDATA=/pgdata -e POSTGRES_DB=pyglass -v $(ROOT)/dbdata/test:/pgdata -v $(ROOT)/postgres_data/test-db:/postgres_data -p5432:5432 -d postgres

# Note that this uses a different host port to the test db, so that both can be running at the same time
run-dev-db:
	@docker run --name dev-postgres -e POSTGRES_PASSWORD=devdb -e PGDATA=/pgdata -e POSTGRES_DB=pyglass -v $(ROOT)/dbdata/dev:/pgdata -v $(ROOT)/postgres_data/dev-db:/postgres_data -p5433:5432 -d postgres

# Launch an EC2 instance that runs the requirements_task worker. This is an on-demand instance and
# it won't shut down until manually terminated. Note this uses a different instance type to the autoscaling group
run-dev-worker:
	@aws ec2 run-instances --instance-type "t2.nano" --security-group-ids "sg-8b6451f8" --key-name "pyglass" --image-id "ami-cd0f5cb6" --region "us-east-1" --subnet-id "subnet-893e3ba5" --associate-public-ip-address --user-data "file://deployment/ec2_bootstrap.sh" --tag-specifications "ResourceType=instance,Tags=[{Key=environment,Value=dev}]" --iam-instance-profile "Name=pyglass-dev"

# Copy EC2 deployment files to S3.
# Set PG_ENV environment variable before running this. eg: PG_ENV=dev make s3-copy-files
s3-copy-files:
	@aws s3 --region us-east-1 cp deployment/requirements_worker_startup.sh "s3://pyglass-${PG_ENV}-resources"
	@aws s3 --region us-east-1 cp deployment/cloudwatch_logs.config "s3://pyglass-${PG_ENV}-resources"
	@aws s3 --region us-east-1 cp .env-${PG_ENV} "s3://pyglass-${PG_ENV}-resources/.env"

dev-web:
	@ENV_FILE=.env-dev FLASK_APP=pyglass/web/app.py FLASK_DEBUG=1 flask run

dev-shell:
	@ENV_FILE=.env-dev IPYTHONDIR=${ROOT}/.ipython ipython

test-shell:
	@ENV_FILE=.env-test ipython

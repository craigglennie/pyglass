from dotenv import load_dotenv, find_dotenv
import os
import warnings


def load_config():
    # PyCharm test configs should set the ENV_FILE environment variable
    env_file_name = os.environ.get('ENV_FILE', '.env')
    env_path = find_dotenv(filename=env_file_name)
    load_dotenv(env_path)


load_config()

# So that None can be passed as a valid default
_no_default = object()


def value(key, default=_no_default):
    result = os.environ.get(key)
    if not result:
        if default != _no_default:
            warnings.warn("No config option in environment for '{key}', "
                          "using supplied default '{default'}")
            return default
        raise KeyError(f"No config value for '{key}'")
    return result

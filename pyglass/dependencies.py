import functools
from enum import IntEnum, Enum
from os import path
import os
from typing import NamedTuple, List, Tuple, Union
from urllib import request
import zipfile
import json
import tempfile

import itertools
from packaging.version import parse
import docker
import attr

from pyglass.common import get_pypi_json, get_logger

logger = get_logger(__name__)


class Containers(Enum):
    PYTHON3 = 'pyglass-py3'
    PYTHON2 = 'pyglass-py2'


class DownloadType(IntEnum):
    WHEEL = 1
    SOURCE = 2


class PythonMajorVersions(IntEnum):
    PYTHON2 = 2
    PYTHON3 = 3


class DownloadOption(NamedTuple):
    url: str
    download_type: DownloadType


@functools.lru_cache()
def cached_get_json(package_name):
    return get_pypi_json(package_name)


class FailedToFindRequirements(Exception):

    def __init__(self, result, message):
        super().__init__(message)
        self.result = result


class MissingReleaseData(Exception):
    def __init__(self, package_name):
        super().__init__(f"Package {package_name}: no release data in Pypi JSON")


class MissingVersionData(Exception):

    def __init__(self, package_name, version):
        super().__init__(f"Package {package_name}: No data for version "
                         f"{version} in release data")


class RequirementSetType(Enum):
    BUILD = 'build'
    RUN = 'run'
    DEV = 'dev'
    TEST = 'test'


@attr.s
class RequirementSet(object):

    set_type = attr.ib(attr.validators.instance_of(RequirementSetType))
    requirements = attr.ib(attr.validators.instance_of(list))
    environment = attr.ib(attr.validators.optional(
        attr.validators.instance_of(str)))


_container_results = ('success', 'timeout', 'setup-error', 'exception')


def _download_file(url: str, directory: str, filename: str):
    destination = path.join(directory, filename)
    with open(destination, 'wb') as outfile:
        with request.urlopen(url) as response:
            outfile.write(response.read())


def get_download_options(package_name: str, version) -> List[DownloadOption]:
    data = cached_get_json(package_name)
    releases = data.get('releases')
    if not releases:
        raise MissingReleaseData(package_name)
    version_data = releases.get(version, None)
    if not version_data:
        raise MissingVersionData(package_name, version)
    options = []
    for download_info in version_data:
        dl_type = (DownloadType.WHEEL
                   if download_info['packagetype'] == 'bdist_wheel'
                   else DownloadType.SOURCE)
        options.append(DownloadOption(download_info["url"], dl_type))
    return sorted(options, key=lambda option: option.download_type)


def requirements_from_metadata(json_data: dict) -> List[RequirementSet]:
    result = []
    for req_type in RequirementSetType:
        key = f'{req_type.value}_requires'
        if key in json_data:
            all_reqs = json_data[key]
            # Packages like beautifulsoup4 4.5.3 have no hard
            # requirements, but do declare extras in 'run', which looks
            # like this:
            # [{'extra': 'html5lib', 'requires': ['html5lib']},
            # {'extra': 'lxml', 'requires': ['lxml']}]
            # ccxt 1.5.95 has version-specific requirements, which enable
            # extra functionality (async stuff). That looks like this:
            # {'environment':'python_version>="3.5"', 'requires': ["aiohttp"]}
            without_extras = [req for req in all_reqs if 'extra' not in req]
            for reqs in without_extras:
                environment = reqs.get('environment', None)
                req_set = RequirementSet(
                    req_type, list(sorted(reqs['requires'])), environment)
                result.append(req_set)
    return result


def requirements_from_wheel(wheel_file: str) -> List[RequirementSet]:

    with zipfile.ZipFile(wheel_file) as _zip:
        json_path = [name.filename for name in _zip.filelist if '.dist-info'
                    in name.filename and 'metadata.json' in name.filename][0]
        with (_zip.open(json_path)) as json_file:
            data = json.load(json_file)
            return requirements_from_metadata(data)


def run_container(container: Containers, package_dir: str, timeout_secs=120) -> Tuple[str, Union[str, None]]:
    """Runs a docker container in order to safely run the setup.py
    file of untrusted packages, and try to find out what they
    declare as their requirements"""
    client = docker.from_env()
    # Mount the package_dir at /package in the container
    volumes = {package_dir: {'bind': '/package', 'mode': 'rw'}}
    bytes_result = client.containers.run(
        image=container.value,
        remove=True,
        volumes=volumes,
        # run.py is copied into place by the Dockerfile
        command=f'python run.py /package {timeout_secs}',
        # Set the docker user to be the same as the user running the container
        # otherwise on Linux the container runs as root and this script
        # can't clean up the files it creates as it's not (shouldn't be!)
        # running as root
        user=os.getuid()
    )
    # We get a byte string back, like b'timeout', so convert it
    result = bytes_result.decode()
    assert result in _container_results, \
        f"Unknown result: '{result}'"
    if result == 'success':
        return result, None
    with open(f'{package_dir}/{result}.output') as error_file:
        error = error_file.read()
        logger.debug(f'Error from {container.value}: {error}')
        return result, error


def process_source_dict(json_path: str) -> List[RequirementSet]:
    with open(json_path) as infile:
        data = json.load(infile)
        reqs = sorted(data.get('install_requires', []))
        # lizard-map 0.13 accidentally(?) passes a one-list tuple, so flatten any lists out
        if len(reqs) and all(isinstance(i, list) for i in reqs):
            reqs = list(itertools.chain(*reqs))
        return [RequirementSet(RequirementSetType.RUN, reqs, None)]


def requirements_from_source(directory: str):
    """Tries to use Python 3 docker, then Python 2 docker, to get
    dependencies from the source code"""
    result, error = run_container(Containers.PYTHON3, directory)
    logger.info(f"Python 3 result: {result}")
    if result == 'success':
        return process_source_dict(f'{directory}/dependencies.json')
    else:
        logger.info("Trying Python 2")
        result, error = run_container(Containers.PYTHON2, directory)
        logger.info(f"Python 2 result: {result}")
        if result == 'success':
            return process_source_dict(f'{directory}/dependencies.json')
    raise FailedToFindRequirements(result, error)


def get_version_requirements(package_name: str, version: str) -> \
        Tuple[DownloadType, List[RequirementSet]]:
    options = get_download_options(package_name, version)
    option = options[0]
    # Need to set 'dir', otherwise on OSX it will create temporary directories
    # in locations (/var) that can't be shared with Docker by default
    with tempfile.TemporaryDirectory(dir='/tmp') as tempdir:
        if option.download_type == DownloadType.WHEEL:
            _download_file(option.url, tempdir, 'wheel.zip')
            logger.info(f'{package_name} {version}: Getting requirements'
                         f' from wheel')
            return DownloadType.WHEEL, requirements_from_wheel(f'{tempdir}/wheel.zip')
        else:
            _download_file(option.url, tempdir, 'source.tar.gz')
            logger.info(f'{package_name} {version}: Getting requirements'
                         f' from source')
            return DownloadType.SOURCE, requirements_from_source(tempdir)


def get_package_requirements(package_name: str, version_count: int=5):
    """Gets requirements info for version_count versions, in descending
    order of version"""
    data = cached_get_json(package_name)
    releases = data.get('releases', None)
    if not releases:
        raise MissingReleaseData(package_name)
    versions = reversed(sorted(map(parse, releases.keys())))
    version_reqs = {}
    errors = {}
    for version in list(versions)[:version_count]:
        try:
            reqs = get_version_requirements(package_name, str(version))
            version_reqs[version] = reqs
        except (MissingReleaseData, MissingVersionData) as err:
            errors[version] = err
    return version_reqs, errors




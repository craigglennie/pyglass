import json
import logging
from urllib.request import urlopen

import boto3

from pyglass import config
from . import config

def get_pypi_json(package_name: str) -> dict:
    url = "https://pypi.python.org/pypi/{package_name}/json".format(
        package_name=package_name)
    return json.load(urlopen(url))


def get_logger(name: str):
    # Should always be called as: get_logger(__name__)

    handlers = [logging.StreamHandler()]
    log_file_path = config.value('LOG_FILE', None)
    if log_file_path:
        handlers.append(logging.FileHandler(log_file_path))

    # This should be a no-op if it has already been called
    logging.basicConfig(level=logging.INFO, datefmt='%Y-%m-%dT%H:%M:%S', handlers=handlers)
    return logging.getLogger(name)


def sqs_result_queue():
    sqs = boto3.resource('sqs', region_name='us-east-1')
    return sqs.get_queue_by_name(
        QueueName=config.value('SQS_REQUIREMENTS_WORKER_RESULT_QUEUE_NAME')
    )


def sqs_task_queue():
    sqs = boto3.resource('sqs', region_name='us-east-1')
    return sqs.get_queue_by_name(
        QueueName=config.value('SQS_REQUIREMENTS_TASK_QUEUE_NAME')
    )

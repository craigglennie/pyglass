from unittest.mock import patch
from urllib.error import HTTPError

import pendulum

from pyglass import config
from pyglass.models import QueuedUpdate, Package, Release, \
    UnresolvedRequirement, Requirement
from pyglass.scripts import process_update_queue
from pyglass.scripts.process_update_queue import resolve_requirements


def test_get_packages_to_update(postgres):
    session = postgres
    utcnow = pendulum.utcnow()
    should_update = QueuedUpdate(package_name='yes',
                                 retry_after=utcnow.subtract(hours=1))
    session.add(should_update)
    dont_update = QueuedUpdate(package_name='no',
                               retry_after=utcnow.add(hours=1))
    session.add(dont_update)
    session.commit()

    [to_update] = process_update_queue._get_packages_to_update()
    assert to_update.id == should_update.id


def test_queued_update_failed(postgres):
    session = postgres
    should_delete = QueuedUpdate(
        package_name='to-delete',
        back_off_factor=config.value("UPDATE_QUEUE_MAX_BACKOFF_FACTOR")
    )
    session.add(should_delete)
    should_increment_backoff = QueuedUpdate(package_name='increment')
    session.add(should_increment_backoff)
    session.commit()

    process_update_queue._queued_update_failed(should_delete)
    process_update_queue._queued_update_failed(should_increment_backoff)

    incremented = session.query(QueuedUpdate).one()
    assert should_increment_backoff.id == incremented.id
    assert incremented.back_off_factor == 1


def test_create_or_update_package(postgres):
    session = postgres
    name, author, home_page, summary = 'package', 'craig', 'http://craigglennie.com', 'summary'
    new_package = process_update_queue._create_or_update_package(name, author,
                                                                 home_page,
                                                                 summary)
    assert new_package.name == name
    assert new_package.author == author
    assert new_package.home_page == home_page
    assert new_package.summary == summary

    new_author = 'tim'
    new_home_page = 'http://tim.com'
    new_summary = 'new summary'
    updated_package = process_update_queue._create_or_update_package(name,
                                                                     new_author,
                                                                     new_home_page,
                                                                     new_summary)

    assert new_package.id == updated_package.id
    from_db = session.query(Package).filter_by(name=name).one()
    assert from_db.name == updated_package.name
    assert from_db.author == updated_package.author
    assert from_db.summary == updated_package.summary
    assert from_db.home_page == updated_package.home_page


def test_order_releases(postgres):
    session = postgres
    package = Package(name='package')
    session.add(package)

    release_1 = Release(package=package, version='1.0', ordering=2)
    release_2 = Release(package=package, version='2.0', ordering=1)
    session.add(release_1)
    session.add(release_2)
    session.commit()

    process_update_queue._order_releases(package)

    first, second = session.query(Release).order_by('ordering').all()
    assert first.ordering == 0
    assert first.id == release_1.id
    assert second.ordering == 1
    assert second.id == release_2.id


def test_resolve_requirements(postgres):
    session = postgres
    queued = QueuedUpdate(package_name='new_package')
    old_package = Package(name='old-package')
    release = Release(package=old_package, version='1.0')
    new_package = Package(name='new-package')
    unresolved = UnresolvedRequirement(release=release, specifier='>=9.0',
                                       queued_update=queued, from_wheel=False,
                                       req_type='run')
    session.add(old_package)
    session.add(release)
    session.add(new_package)
    session.add(queued)
    session.add(unresolved)
    session.commit()

    resolve_requirements([unresolved], new_package)
    assert session.query(UnresolvedRequirement).count() == 0
    req = session.query(Requirement).one()
    assert req.package == new_package
    assert req.release == release
    assert req.specifier == unresolved.specifier


def test_run(postgres):
    session = postgres
    existing_package = Package(name='existing')
    session.add(existing_package)
    release = Release(package=existing_package, version='1.0')
    session.add(release)
    session.commit()

    new_package_name = 'new-package'
    no_version_data_package_name = 'no-version-data'
    no_releases_package_name = 'no-releases'
    can_try_time = pendulum.utcnow().subtract(hours=1)
    updates = [
        QueuedUpdate(
            package_name=existing_package.name,
            retry_after=can_try_time
        ),
        QueuedUpdate(
            package_name=new_package_name,
            retry_after=can_try_time,
            back_off_factor=0
        ),
        QueuedUpdate(
            package_name=no_version_data_package_name,
            retry_after=can_try_time,
            back_off_factor=0
        ),
        QueuedUpdate(
            package_name=no_releases_package_name,
            retry_after=can_try_time,
            back_off_factor=config.value("UPDATE_QUEUE_MAX_BACKOFF_FACTOR")
        )
    ]
    for update in updates:
        session.add(update)
    session.commit()

    existing_new_release_version = '1.5'
    existing_new_release_date = '2017-02-01T15:00:00'

    new_package_release_1_version = '1.0'
    new_package_release_1_date_1 = '2017-02-01T15:00:00'
    new_package_release_1_date_2 = '2017-02-01T16:00:00'
    new_package_release_2_version = '2.0'
    new_package_release_2_date = '2017-02-01T17:00:00'
    new_package_author = 'craig'
    new_package_homepage = 'homepage.com'
    new_package_summary = 'summary'

    def get_mock_json_data(package_name):
        data = {
            existing_package.name: {
                "info": {
                    'name': existing_package.name
                },
                "releases": {
                    '1.0': [
                        {'upload_time': None}
                    ],
                    existing_new_release_version: [
                        {'upload_time': existing_new_release_date}
                    ]}
            },
            new_package_name: {
                'info': {
                    'author': new_package_author,
                    'home_page': new_package_homepage,
                    'summary': new_package_summary,
                    'name': new_package_name
                },
                "releases": {
                    new_package_release_1_version: [
                        # Make sure we're taking the oldest time if there
                        # are multiple
                        {'upload_time': new_package_release_1_date_2},
                        {'upload_time': new_package_release_1_date_1},
                    ],
                    new_package_release_2_version: [
                        {'upload_time': new_package_release_2_date}
                    ]
                }
            },
            no_releases_package_name: {"info": {}},
            no_version_data_package_name: {
                "info": {},
                "releases": {'1.0': []}
            }
        }
        return data[package_name]

    with patch(
            'pyglass.scripts.process_update_queue.get_pypi_json',
            side_effect=get_mock_json_data
    ):
        process_update_queue.run()

    existing_package_release_1, existing_package_release_2 = \
        session.query(Release).filter_by(
            package=existing_package).order_by('ordering')
    assert existing_package_release_2.version == existing_new_release_version
    assert pendulum.parse(existing_new_release_date) == \
           existing_package_release_2.released_at
    assert (existing_package_release_1.ordering <
            existing_package_release_2.ordering)
    assert session.query(QueuedUpdate).filter_by(
        package_name=existing_package.name).count() == 0

    new_package = session.query(Package).filter_by(name=new_package_name).one()
    assert new_package.author == new_package_author
    assert new_package.home_page == new_package_homepage
    new_package_release_1, new_package_release_2 = session.query(
        Release).filter_by(package=new_package).order_by('ordering')
    assert new_package_release_1.version == new_package_release_1_version
    assert new_package_release_2.version == new_package_release_2_version
    assert new_package_release_1.released_at == pendulum.parse(
        new_package_release_1_date_1)
    assert new_package_release_2.released_at == pendulum.parse(
        new_package_release_2_date)
    assert session.query(QueuedUpdate).filter_by(
        package_name=new_package.name).count() == 0

    # Didn't create a package, but will try again
    assert session.query(Package).filter_by(
        name=no_version_data_package_name).count() == 0
    no_version_update = session.query(QueuedUpdate).filter_by(
        package_name=no_version_data_package_name).one()
    assert no_version_update.back_off_factor == 1

    # Should be deleted because it was at max retries
    assert session.query(QueuedUpdate).filter_by(
        package_name=no_releases_package_name).count() == 0
    assert session.query(Package).filter_by(
        name=no_releases_package_name).count() == 0
    assert no_version_update.back_off_factor == 1


def test_run_http_error(postgres):
    session = postgres

    update = QueuedUpdate(package_name='package')
    session.add(update)
    session.commit()

    # def http_error(package_name):
    #     raise HTTPError('http://site.com', 500, 'internal error', None, None)
    error = HTTPError('http://site.com', 500, 'internal error', None, None)
    with patch('pyglass.scripts.process_update_queue.get_pypi_json',
               side_effect=error):
        process_update_queue.run()

    session.refresh(update)
    assert update.back_off_factor == 1

import json
from urllib.error import HTTPError

import boto3
import pytest
from unittest.mock import patch
from pendulum import datetime
from sqlalchemy.exc import IntegrityError

from pyglass import config
from pyglass.dependencies import RequirementSetType, RequirementSet, DownloadType, \
    FailedToFindRequirements, MissingVersionData, MissingReleaseData
from pyglass.models import Release, Package, UnresolvedRequirement, QueuedUpdate, Requirement
from pyglass.scripts import requirements_worker
from pyglass.scripts.link_requirements import get_release_ids_to_process, queue_releases, \
    process_result_queue, link_all, get_or_create_unresolved_requirement, \
    get_or_create_requirement, _queue_release_batch, _get_task_message
from pyglass.scripts.requirements_worker import s3_result_key, requirements_json, error_to_data


def test_error_to_data():
    error = FailedToFindRequirements('result', 'message')
    reason, message = error_to_data(error)
    assert reason == error.result
    assert message == str(error)

    reason, message = error_to_data(MissingVersionData('flask', '0.10'))
    assert reason == 'Missing version data'
    assert not message

    reason, message = error_to_data(MissingReleaseData('flask'))
    assert reason == 'Missing release data'
    assert not message

    error = HTTPError('http://example.com', 404, 'Not Found', None, None)
    reason, message = error_to_data(error)
    assert reason == 'HTTP 404'
    assert message == error.reason

    error = Exception('General error')
    reason, message = error_to_data(error)
    assert reason == error.__class__.__name__
    assert message == str(error)


def test_get_releases_to_process(postgres):
    session = postgres
    package = Package(name='package')
    session.add(package)
    rels = [
        Release(package=package, version='1.0', released_at=datetime(2017, 1, 4)),
        Release(package=package, version='2.0', released_at=datetime(2017, 1, 3),
                requirements_attempt_count=1),
        Release(package=package, version='3.0', released_at=datetime(2017, 1, 2)),
        Release(package=package, version='4.0', released_at=datetime(2017, 1, 1),
                requirements_attempt_count=2),
    ]
    for rel in rels:
        session.add(rel)
    session.commit()

    # Testing max_attempt_count
    release_ids = get_release_ids_to_process(4, 0, False)
    assert release_ids == [rels[0].id, rels[2].id]
    release_ids = get_release_ids_to_process(4, 1, False)
    assert release_ids == [rels[0].id, rels[1].id, rels[2].id]
    release_ids = get_release_ids_to_process(4, 2, False)
    assert release_ids == [i.id for i in rels]

    # Testing max_per_package
    with pytest.raises(AssertionError) as err:
        assert [] == get_release_ids_to_process(0, 0, False)

    release_ids = get_release_ids_to_process(1, 2, False)
    assert release_ids == [rels[0].id]
    release_ids = get_release_ids_to_process(2, 2, False)
    assert release_ids == [rels[0].id, rels[1].id]
    release_ids = get_release_ids_to_process(3, 2, False)
    assert release_ids == [rels[0].id, rels[1].id, rels[2].id]

    # Testing only_new_releases
    package_2 = Package(name='package2')
    rel = Release(package=package_2, version='1.0', released_at=datetime(2017, 1, 4))
    session.add(package)
    session.add(rel)
    session.commit()

    release_ids = get_release_ids_to_process(1, 1, False)
    assert release_ids == [rels[0].id, rel.id]

    # All releases for 'package' are excluded because some have been attempted
    release_ids = get_release_ids_to_process(1, 1, True)
    assert release_ids == [rel.id]


def test_get_or_create(postgres):
    session = postgres
    package = Package(name='package')
    release = Release(package=package, version='1.0')
    queued_update = QueuedUpdate(package_name='queued')
    for obj in [package, release, queued_update]:
        session.add(obj)
    session.commit()

    # First it creates one
    unresolved_1 = get_or_create_unresolved_requirement(
        release, queued_update, 'run', 'env', '1.0'
    )
    unresolved_1.from_wheel = True
    session.add(unresolved_1)
    session.commit()

    # Then it finds the one it just created
    unresolved_2 = get_or_create_unresolved_requirement(
        release, queued_update, 'run', 'env', '1.0'
    )
    assert unresolved_1.id == unresolved_2.id

    package_2 = Package(name='package_2')
    session.add(package_2)
    session.commit()

    req_1 = get_or_create_requirement(release, package_2, 'run', 'env', '1.0')
    req_1.from_wheel = True
    req_1.req_type = 'run'
    session.add(req_1)
    session.commit()

    req_2 = get_or_create_requirement(release, package_2, 'run', 'env', '1.0')
    assert req_1.id == req_2.id


def test_requirements_unique_index(postgres):
    """Postgres unique constraints don't work directly on NULL values, so a common workaround
    is to coalesce the column. Here just confirm that the coalesce part is working"""
    session = postgres

    package = Package(name='package')
    release = Release(package=package, version='1.0')
    # Don't specify 'environment', so that it coalesces to ''
    req = Requirement(release=release, package=package, req_type='run', from_wheel=True)
    queued_update = QueuedUpdate(package_name='queued')
    # Don't specify 'environment', so that it coalesces to ''
    unresolved = UnresolvedRequirement(release=release, queued_update=queued_update, req_type='run',
                                       from_wheel=True)
    for obj in [package, release, req, queued_update, unresolved]:
        session.add(obj)
    session.commit()

    with pytest.raises(IntegrityError) as err:
        session.add(Requirement(release=release, package=package, req_type='run', from_wheel=True))
        session.commit()
        assert 'duplicate key value violates unique constraint' in err.value

    session.rollback()
    # Works fine with a not-null environment
    session.add(Requirement(release=release, package=package, req_type='run', from_wheel=True,
                            environment='env'))
    session.commit()

    with pytest.raises(IntegrityError) as err:
        session.add(UnresolvedRequirement(release=release, queued_update=queued_update,
                                          req_type='run', from_wheel=True))
        session.commit()
        assert 'duplicate key value violates unique constraint' in err.value

    session.rollback()
    # Works fine with a not-null environment
    session.add(UnresolvedRequirement(release=release, queued_update=queued_update, req_type='run',
                                      from_wheel=True, environment='env'))
    session.commit()


def test_queue_releases(moto_task_queue, mocker, postgres):

    mocker.patch('pyglass.scripts.link_requirements.sqs_task_queue', return_value=moto_task_queue)
    session = postgres

    package = Package(name='Flask')
    releases = [Release(package=package, version=i, requirements_attempt_count=0) for i in range(3)]
    session.add_all([package] + releases)
    session.commit()

    queue_releases([release.id for release in releases])
    for release in releases:
        session.refresh(release)
        assert release.is_queued
        assert release.requirements_attempt_count == 1

    messages = moto_task_queue.receive_messages(MaxNumberOfMessages=10, WaitTimeSeconds=10)
    messages.sort(key=lambda m: json.loads(m.body)['version'])
    for message, release in zip(messages, releases):
        expected = _get_task_message(release)
        assert message.body == expected['MessageBody']


def test_sqs_roundtrip(sqs_task_queue, sqs_result_queue, postgres):
    """Full test, roundtripping the task through real SQS, linking results, and checking S3"""
    session = postgres

    assert session.query(UnresolvedRequirement).count() == 0
    assert session.query(QueuedUpdate).count() == 0

    package = Package(name='Flask')
    werkzeug_package = Package(name='Werkzeug')
    # From source
    release_010 = Release(package=package, version='0.10')
    # From wheel
    release_012 = Release(package=package, version='0.12')
    for item in [package, werkzeug_package, release_010, release_012]:
        session.add(item)
    session.commit()

    queue_releases([release_010.id, release_012.id])
    requirements_worker.process_task_queue(False)
    process_result_queue()

    for release in [release_012, release_010]:
        session.refresh(release)
        assert release.error_parsing_requirements is False

    # Leave Werkzeug out of this list, since we already have the package
    expected_queued = [('Jinja2', '>=2.4'), ('itsdangerous', '>=0.21'), ('click', '>=2.0')]
    assert session.query(QueuedUpdate).count() == len(expected_queued)
    updates = session.query(QueuedUpdate).filter(
        QueuedUpdate.package_name.in_([name for name, version in expected_queued])
    ).all()
    assert {obj.package_name for obj in updates} == {name for name, version in expected_queued}

    query = session.query(UnresolvedRequirement).filter_by(release=release_010).all()
    unresolved = {(u.queued_update.package_name, u.specifier) for u in query}
    # Flask 0.10 doesn't depend on 'click'
    assert unresolved == set(expected_queued[:-1])

    query = session.query(UnresolvedRequirement).filter_by(release=release_012).all()
    unresolved = {(u.queued_update.package_name, u.specifier) for u in query}
    assert unresolved == set(expected_queued)

    assert session.query(Requirement).count() == 2
    query = session.query(Requirement).filter_by(package=werkzeug_package)
    for release in [release_010, release_012]:
        req = query.filter_by(release=release).one()
        assert req.specifier == '>=0.7'

    session.refresh(werkzeug_package)
    assert len(werkzeug_package.requirement_of) == 2

    s3 = boto3.resource('s3', region_name='us-east-1')
    bucket_name = config.value('S3_REQUIREMENTS_BUCKET_NAME')

    def check_s3(release, expected_reqs, download_type):
        obj = s3.Object(
            bucket_name,
            s3_result_key(package.name, release.version)
        ).get()
        content = json.load(obj['Body'])
        reqs = [RequirementSet(
            RequirementSetType.RUN,
            expected_reqs,
            None
        )]
        expected = json.loads(requirements_json(package.name, release.version, reqs, download_type))
        assert content == expected

    check_s3(
        release_010,
        ['Jinja2>=2.4', 'Werkzeug>=0.7', 'itsdangerous>=0.21'],
        DownloadType.SOURCE
    )

    check_s3(
        release_012,
        ["Jinja2 (>=2.4)", "Werkzeug (>=0.7)", "click (>=2.0)", "itsdangerous (>=0.21)"],
        DownloadType.WHEEL
    )


def test_multiple_requirements_per_parse(postgres, moto_result_queue, moto_s3, mocker):
    """Tests a case where a single string contains a list of packages, eg in lizard-map 0.13:

    """


def test_version_does_not_exist(postgres, moto_result_queue, moto_s3, mocker):
    mocker.patch('pyglass.scripts.link_requirements.sqs_result_queue', return_value=moto_result_queue)
    mocker.patch('pyglass.scripts.requirements_worker.sqs_result_queue', return_value=moto_result_queue)
    mocker.patch('pyglass.scripts.requirements_worker._s3', return_value=moto_s3)
    session = postgres

    package = Package(name='Flask')

    # Failure
    release_failure = Release(package=package, version='0')
    session.add(package)
    session.add(release_failure)
    session.commit()

    requirements_worker.requirements_task('Flask', '0')
    process_result_queue()
    session.refresh(release_failure)
    assert release_failure.requirements_error_reason == 'Missing version data'
    assert not release_failure.requirements_error_message


def test_malformed_requirements(postgres, moto_result_queue, moto_s3, mocker):
    session = postgres
    mocker.patch('pyglass.scripts.link_requirements.sqs_result_queue', return_value=moto_result_queue)
    mocker.patch('pyglass.scripts.requirements_worker.sqs_result_queue', return_value=moto_result_queue)
    mocker.patch('pyglass.scripts.requirements_worker._s3', return_value=moto_s3)

    # Package exists, but requirements data is malformed. The malformed requirements are for
    # when environment is None, but there's also requirements for a python version range, which
    # are valid. So only the valid requirements should be in the DB. This is the bad requirements
    # string - the trailing comma in the first item breaks the packaging parser:
    # {"requires": ["botocore>=1.3.0,<1.4.0',", "jmespath>=0.7.1,<1.0.0"]}
    # However, the second requirement (jmespath) is valid and should be added.
    # TODO: Flag on release for incomplete requirements?
    # This was addressed in 1.2.1
    bad_boto = Package(name='boto3')
    bad_boto_release = Release(package=bad_boto, version='1.2.0')
    session.add(bad_boto)
    session.add(bad_boto_release)
    session.commit()

    requirements_worker.requirements_task(bad_boto.name, bad_boto_release.version)
    process_result_queue()

    session.refresh(bad_boto_release)
    assert bad_boto_release.error_parsing_requirements is True

    assert session.query(Requirement).filter_by(release=bad_boto_release).count() == 0
    query = session.query(UnresolvedRequirement).filter_by(release=bad_boto_release)

    no_env = query.filter_by(environment=None).one()
    assert no_env.queued_update.package_name == 'jmespath'
    # When the packaging module parses requirements it may reorder the version specifier.
    # Here it changes from the original '>=0.7.1,<1.0.0'
    assert no_env.specifier == '<1.0.0,>=0.7.1'

    has_env = query.filter(UnresolvedRequirement.environment != None).one()
    assert has_env.environment == 'python_version=="2.6" or python_version=="2.7"'
    assert has_env.queued_update.package_name == 'futures'
    assert has_env.specifier == '==2.2.0'


def test_environment_column(postgres, moto_result_queue, moto_s3, mocker):
    """Tests a real package that specifies default 'run' requirements, and also requirements for
    python >= 3.5, where it installs a couple of async packages."""
    session = postgres
    mocker.patch('pyglass.scripts.link_requirements.sqs_result_queue', return_value=moto_result_queue)
    mocker.patch('pyglass.scripts.requirements_worker.sqs_result_queue', return_value=moto_result_queue)
    mocker.patch('pyglass.scripts.requirements_worker._s3', return_value=moto_s3)

    # A package that uses the 'environment' column
    package = Package(name='ccxt')
    release = Release(version='1.5.95', package=package)
    # Setup one of the envrionment packages so that we get both a
    # Requirement and UnresolvedRequirement using the environment column
    resolved_package = Package(name='aiodns')
    for obj in [package, resolved_package, release]:
        session.add(obj)
    session.commit()

    requirements_worker.requirements_task(package.name, release.version)
    process_result_queue()

    resolved = session.query(Requirement).filter_by(package=resolved_package).one()
    assert resolved.environment == 'python_version>="3.5"'

    env_unresolved = session.query(UnresolvedRequirement).filter_by(
        environment='python_version>="3.5"').all()
    assert ['aiohttp', 'cchardet'] == [u.queued_update.package_name for u in env_unresolved]
    assert {'run'} == {u.req_type for u in env_unresolved}


def test_link_all(postgres):
    session = postgres

    package_exists = Package(name='exists')
    queued = QueuedUpdate(package_name='queued')
    package = Package(name='test')
    release = Release(package=package, version='1.0')
    session.add_all([package_exists, queued, package, release])
    session.commit()


    assert release.requirements_attempt_count == 0

    mock_data = [
        RequirementSet(
            RequirementSetType.RUN,
            ['exists (>=2.4)', 'queued (>=0.21)', 'now-queued (>=2.0)'],
            None
        ),
        RequirementSet(RequirementSetType.TEST, ['now-queued-env (>=1.0)'], 'environment')
    ]

    with patch('pyglass.dependencies.get_version_requirements',
               return_value=(DownloadType.WHEEL, mock_data)):
        link_all()
        req = session.query(Requirement).one()
        assert req.package == package_exists
        assert req.release == release
        assert req.specifier == '>=2.4'
        assert req.req_type == 'run'

        assert session.query(UnresolvedRequirement).count() == 3

        unresolved_queued_already = session.query(UnresolvedRequirement).filter(
            UnresolvedRequirement.queued_update_id == queued.id).filter(
            UnresolvedRequirement.environment == None).one()
        assert unresolved_queued_already.release == release
        assert unresolved_queued_already.specifier == '>=0.21'
        assert unresolved_queued_already.from_wheel == True
        assert unresolved_queued_already.req_type == 'run'

        unresolved_new_queued = session.query(UnresolvedRequirement).filter(
            UnresolvedRequirement.queued_update_id != queued.id).filter(
            UnresolvedRequirement.environment == None).one()
        assert unresolved_new_queued.release == release
        assert unresolved_new_queued.specifier == '>=2.0'
        assert unresolved_new_queued.queued_update.package_name == 'now-queued'
        assert unresolved_new_queued.from_wheel == True
        assert unresolved_new_queued.req_type == 'run'

        unresolved_new_queued_with_env = session.query(UnresolvedRequirement).filter(
            UnresolvedRequirement.environment == 'environment').one()
        assert unresolved_new_queued_with_env.specifier == '>=1.0'
        assert unresolved_new_queued_with_env.from_wheel == True
        assert unresolved_new_queued_with_env.queued_update.package_name == 'now-queued-env'
        assert unresolved_new_queued_with_env.req_type == 'test'

        session.refresh(release)
        assert release.requirements_attempt_count == 1

    error_release = Release(package=package, version='2.0')
    session.add(error_release)
    session.commit()

    with patch('pyglass.dependencies.get_version_requirements',
               side_effect=FailedToFindRequirements('setup-error', 'details')):
        link_all(only_new_packages=False)
        session.refresh(error_release)
        assert error_release.requirements_attempt_count == 1
        assert error_release.requirements_error_reason == 'setup-error'
        assert error_release.requirements_error_message == 'details'

        # Test that retry count gets incremented
        link_all(max_attempt_count=1, only_new_packages=False)
        session.refresh(error_release)
        assert error_release.requirements_attempt_count == 2

    http_error_release = Release(package=package, version='3.0')
    session.add(http_error_release)
    session.commit()

    with patch('pyglass.dependencies.get_version_requirements',
               side_effect=HTTPError('http://example.com', 404, 'Not Found', None, None)):
        link_all(only_new_packages=False)
        session.refresh(http_error_release)
        assert http_error_release.requirements_attempt_count == 1
        assert http_error_release.requirements_error_reason == 'HTTP 404'
        assert http_error_release.requirements_error_message == 'Not Found'

    missing_version_release = Release(package=package, version='4.0')
    session.add(missing_version_release)
    session.commit()

    with patch('pyglass.dependencies.get_version_requirements',
               side_effect=MissingVersionData(package.name, '5.0')):
        link_all(only_new_packages=False)
        session.refresh(missing_version_release)
        assert missing_version_release.requirements_attempt_count == 1
        assert missing_version_release.requirements_error_reason == \
               'Missing version data'
        assert missing_version_release.requirements_error_message == None

    non_specific_error_release = Release(package=package, version='5.0')
    session.add(non_specific_error_release)
    session.commit()
    error = AssertionError('something')
    with patch('pyglass.dependencies.get_version_requirements',
               side_effect=error):
        link_all(only_new_packages=False)
        session.refresh(non_specific_error_release)
        assert non_specific_error_release.requirements_attempt_count == 1
        assert non_specific_error_release.requirements_error_reason == \
               'Exception: AssertionError'
        assert non_specific_error_release.requirements_error_message == str(error)

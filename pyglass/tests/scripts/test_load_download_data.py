from collections import namedtuple
from unittest import mock

import pendulum

from pyglass.models import StagedDownload, SuccessfulBigQueryExtraction, \
    Package, Release, Download, QueuedUpdate
from pyglass.scripts import load_download_data


class FakeQuery(object):
    """Mocks a BigQuery query object"""

    def __init__(self):
        self.tokens_received = []

    def run(self):
        pass

    def fetch_data(self, max_results, token):
        self.tokens_received.append(token)
        if len(self.tokens_received) < 3:
            return ['download'] * 10, 10, len(self.tokens_received)
        return ['download'] * 5, 5, None


def test_get_table_names_to_query(postgres):
    session = postgres
    mock_client = mock.Mock()
    bq_tables = ['downloads', 'downloads20170102', 'downloads20170103', 'downloads20170104']
    FakeTable = namedtuple("FakeTable", ["name"])
    mock_client.dataset().list_tables.return_value = [FakeTable(name) for name in bq_tables]

    table_names = load_download_data._get_table_names_to_query(mock_client)
    assert table_names == bq_tables[1:-1]

    session.add(SuccessfulBigQueryExtraction(table_name=bq_tables[1]))
    session.commit()
    table_names = load_download_data._get_table_names_to_query(mock_client)
    assert table_names == bq_tables[2:3]


def test_get_biqquery_sql():
    expected = "SELECT file.project, file.version, details.python, COUNT(*) FROM [the-psf:pypi.downloads20170102] GROUP BY file.project, file.version, details.python ORDER BY file.project, file.version"
    actual = load_download_data._get_bigquery_sql('downloads20170102')
    assert actual == expected


def test_get_rows_from_bigquery():
    mock_client = mock.Mock()
    query = FakeQuery()
    mock_client.run_sync_query.return_value = query
    rows = load_download_data._get_rows_from_bigquery(mock_client, 'table')
    assert rows == ['download'] * 25
    assert query.tokens_received == [None, 1, 2]


def test_copy_staging_to_downloads(postgres):
    session = postgres
    package = Package(name='package')
    session.add(package)
    release = Release(package=package, version='1.0')
    session.add(release)
    session.commit()
    date = pendulum.utcnow().date()

    assert session.query(Download).count() == 0

    copied = StagedDownload(package_name=package.name, package_version=release.version, python_version="2.7", for_date=date, downloads=10)

    not_copied_1 = StagedDownload(package_name=package.name, package_version='2.0', python_version="3.1", for_date=date, downloads=20)

    not_copied_2 = StagedDownload(package_name='no_package', package_version='2.0', python_version='3.4', for_date=date, downloads=30)
    session.add(copied)
    session.add(not_copied_1)
    session.add(not_copied_2)
    session.commit()

    assert 3 == session.query(StagedDownload).count()
    load_download_data._copy_staging_to_downloads()
    download = session.query(Download).one()
    assert download.package == package
    assert download.release == release

    # The copied row should have been removed from staging
    assert 2 == session.query(StagedDownload).count()
    session.refresh(not_copied_1)
    # Set package but there's no release info
    assert not_copied_1.package == package
    assert not_copied_1.release is None
    # Couldn't set package or release
    session.refresh(not_copied_2)
    assert not_copied_2.package is None
    assert not_copied_2.release is None


def test_queue_packages_for_update(postgres):
    session = postgres

    existing = QueuedUpdate(package_name='queued')
    session.add(existing)
    session.commit()

    staged_and_queued = StagedDownload(
        package_name=existing.package_name,
        package_version='1.0',
        for_date=pendulum.utcnow(),
        downloads=1
    )
    staged_not_queued = StagedDownload(
        package_name='not-queued',
        package_version='2.0',
        for_date=pendulum.utcnow(),
        downloads=1
    )
    session.add(staged_and_queued)
    session.add(staged_not_queued)
    session.commit()

    load_download_data._queue_packages_for_update()

    queued, already_queued = session.query(QueuedUpdate).order_by('package_name').all()
    assert already_queued.package_name == existing.package_name
    assert queued.package_name == staged_not_queued.package_name


@mock.patch('pyglass.scripts.load_download_data._get_rows_from_bigquery')
@mock.patch('pyglass.scripts.load_download_data._get_table_names_to_query')
def test_run(_get_table_names_to_query, _get_rows_from_bigquery, postgres):

    session = postgres

    _get_table_names_to_query.return_value = ('downloads20180203',)
    _get_rows_from_bigquery.return_value = (
        ('package1', '1.0', '2.7', 10),
        # The caosz library has a weird version string, with commas
        # That's why we used TSV not CSV for the COPY FROM stage
        # (avoids having to deal with quoting)
        ('package1', '2,0,0', '3.1', 20),
        ('package2', '3.0', '', 30),
    )
    package = Package(name='package1')
    session.add(package)
    release = Release(package=package, version='1.0')
    session.add(release)
    session.commit()

    assert 0 == session.query(Download).count()
    load_download_data.run()

    download = session.query(Download).one()
    assert download.package == package
    assert download.release == release
    assert download.python_version == '2.7'
    assert download.for_date == pendulum.date(2018, 2, 3)
    assert download.downloads == 10

    staging1, staging2 = session.query(StagedDownload).order_by('package_name').all()
    assert staging1.package_name == 'package1'
    assert staging1.package_version == '2,0,0'
    assert staging1.python_version == '3.1'
    assert staging1.downloads == 20
    assert staging2.package_name == 'package2'
    assert staging2.package_version == '3.0'
    assert staging2.python_version is None
    assert staging2.downloads == 30

    update1, update2 = session.query(QueuedUpdate).order_by('package_name').all()
    assert update1.package_name == 'package1'
    assert update2.package_name == 'package2'

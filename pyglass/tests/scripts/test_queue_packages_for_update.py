from unittest.mock import patch

from pyglass.models import QueuedUpdate, Release, Package
from pyglass.scripts import queue_packages_for_update


def test_run(postgres):
    session = postgres
    package_existing = Package(name='existing')
    release = Release(package=package_existing, version='1.0')
    session.add(package_existing)
    session.add(release)
    session.commit()

    mock_data = [
        {'link': f'http://pypi.python.org/pypi/{package_existing.name}/{release.version}'},
        {'link': 'http://pypi.python.org/pypi/newpackage/1.0'},
    ]

    # TODO: Test back-off factor update

    with patch('pyglass.scripts.queue_packages_for_update._get_rss_entries', return_value=mock_data):
        queue_packages_for_update.run()

        newpackage = session.query(QueuedUpdate).one()
        assert 0 == newpackage.back_off_factor

        # Now a new version is released, so we should enqueue an update for it
        mock_data[0]['link'] = f'http://pypi.python.org/pypi/{package_existing.name}/2.0'
        queue_packages_for_update.run()

        assert session.query(QueuedUpdate).count() == 2
        new_queued_update = session.query(QueuedUpdate).filter_by(package_name=package_existing.name).one()
        assert 0 == new_queued_update.back_off_factor

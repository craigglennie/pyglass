import json
import uuid
from unittest.mock import patch

import boto3
from pendulum import time

from pyglass import config
from pyglass.dependencies import RequirementSet, RequirementSetType, DownloadType
from pyglass.scripts.requirements_worker import s3_result_key, requirements_task, \
    requirements_json, WorkerResult
from pyglass.common import sqs_result_queue


def test_requirements_task(sqs_queue):
    mock_data = [
        RequirementSet(
            RequirementSetType.RUN,
            ['flask (>=2.4)'],
            None
        ),
    ]

    package_name, version = str(uuid.uuid4()), '1.0'
    s3 = boto3.resource('s3')
    bucket_name = config.value('S3_REQUIREMENTS_BUCKET_NAME')
    obj = s3.Object(
        bucket_name,
        s3_result_key(package_name, version)
    )

    with patch('pyglass.dependencies.get_version_requirements',
               return_value=(DownloadType.WHEEL, mock_data)):
        requirements_task.apply(args=(package_name, version))

    try:
        raw = obj.get()['Body'].read()
    except s3.exceptions.NoSuchKey:
        time.sleep(5)
        raw = obj.get()['Body'].read()

    expected = json.loads(requirements_json(package_name, version, mock_data, DownloadType.WHEEL))
    assert json.loads(raw) == expected
    # TODO: Always clean up the objects after the test has run? Or just nuke the bucket contents
    # when the tests start, or when it gets too much stuff in it
    obj.delete()

    [message] = sqs_queue.receive_messages(MaxNumberOfMessages=1, WaitTimeSeconds=20)
    expected['result'] = WorkerResult.SUCCESS.value
    assert json.loads(message.body) == expected
    message.delete()

    # TODO: Test failure message.
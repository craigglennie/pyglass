from sqlalchemy.orm import Session
from pyquery import PyQuery

from pyglass.models import Package, Release
from pendulum import Pendulum
from pyglass.web.app import app


def test_package_page(postgres: Session):

    app.testing = True
    client = app.test_client()

    session = postgres

    package = Package(name='Flask', summary='Package summary')
    release_1 = Release(package=package, version='1.0',
                        released_at=Pendulum(2017, 1, 1))
    release_2 = Release(package=package, version='2.0',
                        released_at=Pendulum(2017, 1, 2))
    for obj in [package, release_1, release_2]:
        session.add(obj)
    session.commit()

    result = client.get('/package/Flask')
    pq = PyQuery(result.data)
    assert pq("h1")[0].text == package.name
    assert pq("h3")[0].text == package.summary
    assert pq("h3")[1].text == 'Current Version: 2.0 - Released 2017-01-02 00:00 (UTC)'
    # render_template.assert_called_once_with('package.html', package=package,
    #                                         releases=[release_2, release_1])



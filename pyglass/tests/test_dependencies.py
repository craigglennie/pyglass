import json
import tempfile
from unittest.mock import patch
import shutil

from packaging.version import Version

from pyglass.dependencies import get_download_options, cached_get_json, \
    _download_file, MissingVersionData, DownloadType, requirements_from_wheel, \
    run_container, process_source_dict, requirements_from_source, \
    DownloadOption, get_version_requirements, Containers, MissingReleaseData, \
    FailedToFindRequirements, get_package_requirements, RequirementSet, \
    RequirementSetType
import pytest


def _resource_path(filename):
    return f'pyglass/tests/resources/{filename}'


@patch('pyglass.dependencies.request')
def test_download_file(request):
    data = b'1234'
    with tempfile.TemporaryDirectory() as tempdir:
        with open(f'{tempdir}/input.file', 'wb') as write_input:
            write_input.write(data)
            write_input.flush()
            with open(f'{tempdir}/input.file', 'rb') as read_input:
                request.urlopen.return_value = read_input
                url = "http://example.com/file"
                output_file = 'output.file'
                _download_file(url, tempdir, output_file)
                request.urlopen.assert_called_with(url)
                with open(f'{tempdir}/{output_file}', 'rb') as infile:
                    assert infile.read() == data


@patch('pyglass.dependencies.cached_get_json')
def test_get_download_options(get_json):

    with pytest.raises(MissingReleaseData) as excinfo:
        get_json.return_value = {}
        get_download_options('package', '1.0')
        get_json.assert_called_once_with('package')
        assert 'No release data' in excinfo.value

    get_json.reset_mock()

    with pytest.raises(MissingVersionData) as excinfo:
        # Has version data, but not for this version we're interested in
        get_json.return_value = {'releases': {'0.5': {}}}
        get_download_options('package', '1.0')
        get_json.assert_called_once_with('package')
        assert 'No release data' in excinfo.value

    get_json.return_value = {
        'releases': {
            '1.0': [
                {'packagetype': 'sdist', 'url': 'source.tar.gz'},
                {'packagetype': 'bdist_wheel', 'url': 'wheel.zip'}
            ]
        }
    }

    with pytest.raises(MissingVersionData) as excinfo:
        get_download_options('package', '2.0')
        assert 'No data for version' in excinfo.value

    opt_wheel, opt_source = get_download_options('package', '1.0')
    assert opt_wheel.url == 'wheel.zip'
    assert opt_wheel.download_type == DownloadType.WHEEL
    assert opt_source.url == 'source.tar.gz'
    assert opt_source.download_type == DownloadType.SOURCE


def test_requirements_from_wheel():
    # Test with all build options (except meta) filled in, though I don't know
    # if this ever actually happens. So this was constructed by referring to
    # http://python.org/dev/peps/pep-0426/#requirement-specifiers
    wheel_file = _resource_path('mock-metadata.whl')
    reqs = requirements_from_wheel(wheel_file)
    expected = [
        RequirementSet(RequirementSetType.BUILD, ['BuildPackage (>=1.0)'], None),
        RequirementSet(RequirementSetType.RUN, ['RunPackage (>=1.0)'], None),
        RequirementSet(RequirementSetType.DEV, ['DevPackage (>=1.0)'], None),
        RequirementSet(RequirementSetType.TEST, ['TestPackage (>=1.0)'], None)
    ]
    assert reqs == expected

    # Test against real Flask version
    wheel_file = _resource_path('Flask-0.12.2-py2.py3-none-any.whl')
    reqs = requirements_from_wheel(wheel_file)
    expected = [
        RequirementSet(RequirementSetType.RUN,
                       ["Jinja2 (>=2.4)", "Werkzeug (>=0.7)",
                        "click (>=2.0)", "itsdangerous (>=0.21)"],
                       None)
    ]
    assert reqs == expected

    # beautifulsoup4 4.5.3 which has extras, but no requirements
    wheel_file = _resource_path('beautifulsoup4-4.5.3-py3-none-any.whl')
    reqs = requirements_from_wheel(wheel_file)
    assert not reqs

    # ccxt 1.5.95 has environment requirements for python>=3.5
    wheel_file = _resource_path('ccxt-1.5.95-py2.py3-none-any.whl')
    reqs = requirements_from_wheel(wheel_file)
    expected = [
        RequirementSet(RequirementSetType.RUN, ['setuptools'], None),
        RequirementSet(RequirementSetType.RUN,
                       ['aiodns', 'aiohttp', 'cchardet'],
                       'python_version>="3.5"')
    ]
    assert reqs == expected


def test_run_container():

    def copy_to_docker(source_path: str, dest_dir: str):
        shutil.copy(source_path, f'{dest_dir}/source.tar.gz')

    for container in (Containers.PYTHON3, Containers.PYTHON2):

        # Test with a real package - Flask
        # If this blows up you may not have built the Docker container.
        # Use the Makefile in pyglass/docker
        with tempfile.TemporaryDirectory(dir='/tmp') as tempdir:
            copy_to_docker(_resource_path('Flask-0.12.2.tar.gz'), tempdir)
            status, error = run_container(container, tempdir)
            assert status == 'success'
            assert error is None
            reqs = process_source_dict(f'{tempdir}/dependencies.json')

            expected = [RequirementSet(RequirementSetType.RUN,
                                       ["Jinja2>=2.4", "Werkzeug>=0.7",
                                        "click>=2.0", "itsdangerous>=0.21"],
                                       None)]
            assert reqs == expected

        # Test exception handling by passing a non-int value as the timeout
        with tempfile.TemporaryDirectory(dir='/tmp') as tempdir:
            copy_to_docker(_resource_path('Flask-0.12.2.tar.gz'), tempdir)
            status, error = run_container(container, tempdir, timeout_secs='NaN')
            assert status == 'exception'
            assert 'Traceback' in error and 'NaN' in error
            with open(f'{tempdir}/exception.output') as infile:
                assert 'Traceback' in infile.read()

        # Test timeout of the setup script
        with tempfile.TemporaryDirectory(dir='/tmp') as tempdir:
            copy_to_docker(_resource_path('source-timeout.tar.gz'), tempdir)
            status, error = run_container(container, tempdir, timeout_secs=5)
            # There's nothing else to say about a timeout
            assert status == 'timeout'
            assert error == ''

        # Test an error that occurs in the setup script, not our code
        with tempfile.TemporaryDirectory(dir='/tmp') as tempdir:
            copy_to_docker(_resource_path('source-setup-error.tar.gz'), tempdir)
            status, error = run_container(container, tempdir)
            assert status == 'setup-error'
            assert 'Traceback' in error and 'Test Exception' in error
            with open(f'{tempdir}/setup-error.output') as infile:
                assert 'Traceback' in infile.read()

    # Test that the Python 2 container works for things that don't work in Py3
    with tempfile.TemporaryDirectory(dir='/tmp') as tempdir:
        copy_to_docker(_resource_path('py27-only.tar.gz'), tempdir)

        # Test it really fails under Python 3
        status, error = run_container(Containers.PYTHON3, tempdir)
        assert status == 'setup-error'

        # But works under Python 2
        status, error = run_container(Containers.PYTHON2, tempdir)
        assert status == 'success'
        assert error is None
        reqs = process_source_dict(f'{tempdir}/dependencies.json')
        expected = [RequirementSet(RequirementSetType.RUN, ['python2-only'],
                                   None)]
        assert reqs == expected


def test_requirements_from_source():
    with tempfile.TemporaryDirectory(dir='/tmp') as tempdir:
        shutil.copy(_resource_path('py27-only.tar.gz'), f'{tempdir}/source.tar.gz')
        reqs = requirements_from_source(tempdir)
        expected = [RequirementSet(RequirementSetType.RUN, ['python2-only'],
                                   None)]
        assert reqs == expected

    with tempfile.TemporaryDirectory(dir='/tmp') as tempdir:
        shutil.copy(_resource_path('source-setup-error.tar.gz'), f'{tempdir}/source.tar.gz')
        with pytest.raises(FailedToFindRequirements) as excinfo:
            reqs = requirements_from_source(tempdir)
            assert excinfo.value.result == 'setup-error'


@patch('pyglass.dependencies.get_download_options')
def test_get_version_requirements(download_options):

    # 'download' a file from local testing data to the destination
    def copy_test_file(src_path, tempdir, name):
        shutil.copyfile(src_path, f'{tempdir}/{name}')

    options = [
        DownloadOption(_resource_path('Flask-0.12.2-py2.py3-none-any.whl'),
                       DownloadType.WHEEL),
        DownloadOption(_resource_path('Flask-0.12.2.tar.gz'), DownloadType.SOURCE)
    ]
    download_options.return_value = options

    with patch('pyglass.dependencies._download_file', new=copy_test_file),\
        patch('pyglass.dependencies.requirements_from_wheel',
             wraps=requirements_from_wheel) as wheel_spy,\
        patch('pyglass.dependencies.requirements_from_source',
              wraps=requirements_from_source) as source_spy:

        dl_type, reqs = get_version_requirements('flask', '0.12.2')
        assert dl_type == DownloadType.WHEEL
        wheel_spy.assert_called_once()
        source_spy.assert_not_called()
        expected = RequirementSet(RequirementSetType.RUN, [
            "Jinja2 (>=2.4)",
            "Werkzeug (>=0.7)",
            "click (>=2.0)",
            "itsdangerous (>=0.21)",
        ], None)
        assert reqs == [expected]

        options.pop(0)

        dl_type, reqs = get_version_requirements('flask', '0.12.2')
        assert dl_type == DownloadType.SOURCE
        # This would fail if it had been called again
        wheel_spy.assert_called_once()
        source_spy.assert_called_once()
        expected = RequirementSet(RequirementSetType.RUN, [
            "Jinja2>=2.4",
            "Werkzeug>=0.7",
            "click>=2.0",
            "itsdangerous>=0.21",
        ], None)
        assert reqs == [expected]


@patch('pyglass.dependencies.cached_get_json')
def test_get_package_requirements(get_json):

    get_json.return_value = {}
    with pytest.raises(MissingReleaseData):
        get_package_requirements('Flask')

    with open(_resource_path('flask-pypi-json.json')) as infile:
        pypi_json = json.load(infile)
    # Add a version with no data, in order to get a MissingVersionData error
    pypi_json['releases']['0.13'] = {}
    get_json.return_value = pypi_json

    # 'download' a file from local testing data to the destination
    def copy_test_file(url, tempdir, name):
        local_filename = url.split('/')[-1]
        shutil.copyfile(_resource_path(local_filename), f'{tempdir}/{name}')

    with patch('pyglass.dependencies._download_file', new=copy_test_file):
        reqs, errors = get_package_requirements('flask', version_count=3)

        [error_version] = errors.keys()
        assert isinstance(error_version, Version)
        assert str(error_version) == '0.13'
        error = errors[error_version]
        assert isinstance(error, MissingVersionData)

        [version_1, version_2] = sorted(reqs.keys())
        assert isinstance(version_1, Version) and str(version_1) == '0.12.1'
        assert isinstance(version_2, Version) and str(version_2) == '0.12.2'
        for download_type, version_reqs in reqs.values():
            assert download_type == DownloadType.WHEEL
            expected = RequirementSet(RequirementSetType.RUN, [
                "Jinja2 (>=2.4)",
                "Werkzeug (>=0.7)",
                "click (>=2.0)",
                "itsdangerous (>=0.21)",
            ], None)
            assert version_reqs == [expected]


def test_process_source_dict():
    reqs = [1, 2]
    with tempfile.NamedTemporaryFile('w') as json_file:
        json.dump({'install_requires': reqs}, json_file)
        json_file.flush()
        result = process_source_dict(json_file.name)[0]
        assert result.requirements == reqs

    # lizard-map 0.13 accidentally(?) passes a single-list tuple ([1,2],) which should be flattened
    reqs = ([1, 2], [3, 4])
    with tempfile.NamedTemporaryFile('w') as json_file:
        json.dump({'install_requires': reqs}, json_file)
        json_file.flush()
        result = process_source_dict(json_file.name)[0]
        assert result.requirements == [1, 2, 3, 4]



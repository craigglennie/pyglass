import pytest
from pyglass import config


def test_value():
    assert 'postgresql' in config.value('DB_CONNECTION_STRING')
    assert 'use_default' in config.value('NOT_FOUND', 'use_default')
    with pytest.raises(KeyError) as err:
        config.value('NO_DEFAULT')
        assert 'No config value' in err.value
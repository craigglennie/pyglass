import boto3
import moto
import pytest
import os
from alembic.config import Config
from alembic import command
from sqlalchemy.exc import ProgrammingError

from pyglass import common
from pyglass.db import session
from pyglass.models import Base
from pyglass import config


def _delete_all(session):
    for table in reversed(Base.metadata.sorted_tables):
        try:
            session.execute(f"DELETE FROM {table.name}")
            session.commit()
        except ProgrammingError as error:
            if 'relation' in str(error) and 'does not exist' in str(error):
                session.rollback()
                continue


@pytest.fixture(scope='module')
def postgres_session():

    if 'test' not in config.value('DB_CONNECTION_STRING'):
        raise AssertionError("Aborting tests - you might not be "
                             "running against a test database")

    # session = get_or_create_session()
    _delete_all(session)

    # Run Alembic migrations, expecting the alembic.ini file to be
    # at the project root. Also, update script_location (the directory
    # with the migrations in it) to be an absolute path
    this_path = __name__.replace('.', '/')
    root_path = __file__.replace('.py', '').replace(this_path, '')
    config_path = os.path.join(root_path, 'alembic.ini')
    cfg = Config(config_path)
    script_location = cfg.get_main_option('script_location')
    cfg.set_main_option('script_location', os.path.join(root_path, script_location))
    command.downgrade(cfg, 'base')
    command.upgrade(cfg, 'head')
    return session


@pytest.fixture
def postgres(postgres_session):
    postgres_session.rollback()
    _delete_all(postgres_session)
    postgres_session.expunge_all()
    return postgres_session


@pytest.fixture()
def sqs_task_queue():
    queue = common.sqs_task_queue()

    # Don't use the PurgeQueue operation because it has a once-in-60-seconds limitation
    # that causes tests to fail.
    def clear_queue():
        while 1:
            messages = queue.receive_messages(MaxNumberOfMessages=10)
            if not messages:
                break
            queue.delete_messages(
                Entries=[{
                    'Id': m.message_id,
                    'ReceiptHandle': m.receipt_handle
                } for m in messages]
            )

    clear_queue()
    yield queue
    clear_queue()


@pytest.fixture()
def sqs_result_queue():
    queue = common.sqs_result_queue()

    # Don't use the PurgeQueue operation because it has a once-in-60-seconds limitation
    # that causes tests to fail.
    def clear_queue():
        while 1:
            messages = queue.receive_messages(MaxNumberOfMessages=10)
            if not messages:
                break
            queue.delete_messages(
                Entries=[{
                    'Id': m.message_id,
                    'ReceiptHandle': m.receipt_handle
                } for m in messages]
            )

    clear_queue()
    yield queue
    clear_queue()


@pytest.fixture()
def moto_result_queue():
    with moto.mock_sqs():
        sqs = boto3.resource('sqs', region_name='us-east-1')
        yield sqs.create_queue(QueueName='mock-result-queue')


@pytest.fixture()
def moto_task_queue():
    with moto.mock_sqs():
        sqs = boto3.resource('sqs', region_name='us-east-1')
        yield sqs.create_queue(QueueName='mock-task-queue')


@pytest.fixture()
def moto_s3():
    with moto.mock_s3():
        s3 = boto3.resource('s3', region_name='us-east-1')
        s3.create_bucket(Bucket=config.value('S3_REQUIREMENTS_BUCKET_NAME'))
        yield s3

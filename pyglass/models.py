from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy import Column, Integer, Date, String, ForeignKey, \
    UniqueConstraint, Enum, Boolean
from sqlalchemy.orm import relationship
from sqlalchemy.sql import expression
from sqlalchemy.ext.compiler import compiles
from sqlalchemy.types import DateTime

__all__ = ['QueuedUpdate', 'Package', 'Release', 'Requirement',
           'UnresolvedRequirement', 'Download', 'StagedDownload',
           'SuccessfulBigQueryExtraction']

Base = declarative_base()


# Have postgres set the timestamp with UTC timezone, rather than having
# SqlAlchemy do it, or having the database do it without the timezone
# conversion. The database-server should be on UTC anyway, but in case it's
# not this will still set a UTC timestamp
class utcnow(expression.FunctionElement):
    type = DateTime()


@compiles(utcnow, 'postgresql')
def pg_utcnow(element, compiler, **kw):
    return "TIMEZONE('utc', CURRENT_TIMESTAMP)"


class QueuedUpdate(Base):
    """Represents a queue for packages that need to have the latest release
    info pulled from Pypi"""
    __tablename__ = "queued_updates"

    id = Column(Integer, primary_key=True)
    created_at = Column(DateTime, nullable=False, server_default=utcnow())
    updated_at = Column(DateTime, nullable=True, default=None)
    package_name = Column(String(100), unique=True)
    back_off_factor = Column(Integer, default=0)
    retry_after = Column(DateTime, server_default=utcnow())
    unresolved_requirements = relationship("UnresolvedRequirement",
                                           backref="queued_update")

    def __repr__(self):
        return (f"<QueuedUpdate {self.id} package:{self.package_name} " 
                f"backoff:{self.back_off_factor}")


class Package(Base):
    __tablename__ = "packages"
    # Can't find an official rule, but the longest package name I found in
    # BigQuery was 58 chars
    id = Column(Integer, primary_key=True)
    created_at = Column(DateTime, nullable=False, server_default=utcnow())
    updated_at = Column(DateTime, nullable=True, default=None)
    name = Column(String(100), unique=True)
    author = Column(String)
    home_page = Column(String(200))
    summary = Column(String)
    releases = relationship("Release", backref="package")
    downloads = relationship("Download", backref="package")
    staging_downloads = relationship("StagedDownload", backref="package")
    requirement_of = relationship('Requirement', backref="package")

    def __repr__(self):
        return f"<Package {self.id} '{self.name}'>"


class Release(Base):
    __tablename__ = "releases"

    id = Column(Integer, primary_key=True)
    created_at = Column(DateTime, nullable=False, server_default=utcnow())
    updated_at = Column(DateTime, nullable=True, default=None)
    package_id = Column(Integer, ForeignKey('packages.id'))
    # TODO: Should we pass and retry on versions without
    # release dates? If so release_at shouldn't be nullable
    # For some reason release information is sometimes missing from pypi
    # JSON data, which means we can't get an upload_time
    # (which seems weird, since I thought pypi was generating it).
    # See https://pypi.python.org/pypi/mxnet/json
    released_at = Column(DateTime)
    # Need to accommodate some whacky long version names
    version = Column(String(100), index=True)
    # Allows for the database to compare versions of a package without understanding
    # PEP440 versioning rules. T his ID can be generated by sorting a list of
    # packaging.Version objects.
    ordering = Column(Integer)
    # Whether the release is currently enqueued for requirements gathering
    is_queued = Column(Boolean, default=False, nullable=False)
    # This flag is intended to allow the UI to show when requirements info for a release
    # may be incomplete. This can happen (eg for boto3 1.2.0) when there is a malformed
    # requirements string in the requirement set, meaning that not all requirements can be
    # parsed and linked  to the release
    error_parsing_requirements = Column(Boolean, nullable=True, default=None)
    requirements_attempt_count = Column(Integer, default=0, nullable=False)
    requirements_error_reason = Column(String(20), nullable=True, default=None)
    requirements_error_message = Column(String, nullable=True, default=None)
    downloads = relationship("Download", backref="release")
    requirements = relationship("Requirement", backref="release",
                                foreign_keys='Requirement.release_id')
    unresolved_requirements = relationship("UnresolvedRequirement",
                                           backref="release")
    staging_downloads = relationship("StagedDownload", backref="release")

    class Meta:
        unique_together = ('package', 'version')

    def __repr__(self):
        return (f"<Release {self.id}, package:{self.package.name}, "
                f"version:{self.version}>")


class Requirement(Base):
    """Represents a package that is required for a Release. Includes the
    PEP440 version specifier, which is compatible with the 'packaging'
    modules SpecifierSet class, and is used to create a set of requirement
    links."""
    __tablename__ = "requirements"

    id = Column(Integer, primary_key=True)
    created_at = Column(DateTime, nullable=False, server_default=utcnow())
    updated_at = Column(DateTime, nullable=True, default=None)
    release_id = Column(Integer, ForeignKey('releases.id'), nullable=False)
    package_id = Column(Integer, ForeignKey('packages.id'), nullable=False)
    from_wheel = Column(Boolean, nullable=False)
    environment = Column(String(20), nullable=True, default=None)
    req_type = Column(Enum('run', 'test', 'build', 'dev'), nullable=False)
    specifier = Column(String, nullable=True)

    def __repr__(self):
        return (f'<Requirement {self.id} {self.release.package.name} '
                f'{self.release.version} requires {self.package.name} '
                f'{self.specifier}>')


class UnresolvedRequirement(Base):
    """Represents a requirement on a package that we do not have information
    about yet."""
    __tablename__ = "unresolved_requirements"

    id = Column(Integer, primary_key=True)
    created_at = Column(DateTime, nullable=False, server_default=utcnow())
    updated_at = Column(DateTime, nullable=True, default=None)
    release_id = Column(Integer, ForeignKey('releases.id'), nullable=False)
    specifier = Column(String, nullable=True)
    from_wheel = Column(Boolean, nullable=False)
    environment = Column(String(20), nullable=True, default=None)
    req_type = Column(Enum('run', 'test', 'build', 'dev'), nullable=False)
    queued_update_id = Column(
        Integer, ForeignKey('queued_updates.id'), nullable=False)

    def __repr__(self):
        return (f'<UnresolvedRequirement {self.id} '
                f'{self.release.package.name}:{self.release.version} '
                f'-> {self.queued_update.package_name}>')


class Download(Base):
    __tablename__ = "downloads"

    id = Column(Integer, primary_key=True)
    created_at = Column(DateTime, nullable=False, server_default=utcnow())
    updated_at = Column(DateTime, nullable=True, default=None)
    # TODO: Get rid of this, have Package -> Release -> Downloads
    package_id = Column(Integer, ForeignKey('packages.id'), nullable=False)
    release_id = Column(Integer, ForeignKey('releases.id'), nullable=False)
    # Max version length seen in BigQuery was 9 chars: 2.7.11rc1
    # Allow for (unlikely) many release candidates.
    python_version = Column(String(20), index=True)
    # for_date avoids conflicting with a reserved word in postgres
    for_date = Column(Date, nullable=False)
    downloads = Column(Integer, nullable=False)

    def __repr__(self):
        return (f"{self.id}, package: {self.package.name}, "
                f"date: {self.for_date}, downloads: {self.downloads}")


# TODO: Rename to StagedDownload ?
class StagedDownload(Base):
    """A table for staging bigquery data into the downloads table"""
    __tablename__ = 'staging_downloads'

    id = Column(Integer, primary_key=True)
    created_at = Column(DateTime, nullable=False, server_default=utcnow())
    updated_at = Column(DateTime, nullable=True, default=None)
    package_name = Column(String(100), index=True, nullable=False)
    package_id = Column(Integer, ForeignKey('packages.id'), nullable=True)
    # TODO: Rename to release_version?
    package_version = Column(String(100), nullable=False)
    release_id = Column(Integer, ForeignKey('releases.id'), nullable=True)
    python_version = Column(String(20), nullable=True)
    for_date = Column(Date, nullable=False)
    downloads = Column(Integer, nullable=False)

    UniqueConstraint('package_name', 'package_version', 'python_version',
                     'for_date')


class SuccessfulBigQueryExtraction(Base):
    __tablename__ = "successful_bigquery_extractions"

    created_at = Column(DateTime, nullable=False, server_default=utcnow())
    updated_at = Column(DateTime, nullable=True, default=None)
    table_name = Column(String(17), primary_key=True)
    completed_at = Column(DateTime, server_default=utcnow())




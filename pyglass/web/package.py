from typing import List
from flask import render_template, abort

from pyglass.models import Release, Package
from pyglass.db import session


def get_releases(package: Package) -> List[Release]:
    return session.query(Release).filter_by(package=package)\
        .order_by(Release.released_at.desc()).all()


def package_page(name):
    package = session.query(Package).filter_by(name=name).one_or_none()
    if not package:
        abort(404, package_name=name)

    releases = get_releases(package)
    # If we don't have any release info then there's really nothing to say yet
    if not releases:
        abort(404, package_name=name)

    template_data = {
        'package': package,
        'releases': releases
    }

    return render_template('package.html', **template_data)

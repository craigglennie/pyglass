from flask import render_template

from pyglass.db import session
from pyglass.models import Package, Release, Download, StagedDownload, QueuedUpdate, Requirement, \
    UnresolvedRequirement


def stats_page():
    template_data = {
        'package_count': session.query(Package).count(),
        'release_count': session.query(Release).count(),
        'downloads_row_count': session.query(Download).count(),
        'staged_downloads_row_count': session.query(StagedDownload).count(),
        'queued_updates_row_count': session.query(QueuedUpdate).count(),
        'requirements_count': session.query(Requirement).count(),
        'unresolved_requirements_count': session.query(UnresolvedRequirement).count(),
    }
    return render_template('stats.html', **template_data)

from datetime import timedelta
from typing import List, Tuple

import itertools
from flask import render_template
from packaging.requirements import SpecifierSet
from packaging.version import parse
from pendulum import Pendulum
from sqlalchemy.orm import aliased
from sqlalchemy import func
from collections import defaultdict, namedtuple

from pyglass.models import Release, Requirement, UnresolvedRequirement, Package, Download
from pyglass.db import session


def get_requirements(release: Release) -> List[Requirement]:
    results = session.query(Requirement).filter_by(release=release).all()
    results.sort(key=lambda x: x.package.name)
    return results


def get_download_count(start: Pendulum, end: Pendulum, release: Release) -> int:
    query = session.query(func.sum(Download.downloads)).filter_by(release=release)
    query = query.filter(Download.for_date >= start).filter(Download.for_date <= end)
    return query.one()[0]


def get_unresolved_requirements(release: Release) -> List[UnresolvedRequirement]:
    results = session.query(UnresolvedRequirement).filter_by(release=release).all()
    results.sort(key=lambda x: x.queued_update.package_name)
    return results


Requiree = namedtuple('Requiree', ['name', 'description', 'versions'])


def get_requirees(release: Release) -> List[Requiree]:
    def in_spec(spec, version):
        return not spec or SpecifierSet(spec).contains(version)

    reqs = session.query(Requirement.release_id, Requirement.specifier) \
        .filter_by(package=release.package).all()

    have_dep = [release_id for release_id, spec in reqs if in_spec(spec, release.version)]
    rows = session.query(Release.version, Package.name, Package.summary)\
        .join(Package)\
        .filter(Release.id.in_(have_dep))\
        .order_by(Package.name)\
        .all()

    groups = {}
    for key, group in itertools.groupby(rows, lambda row: (row[1], row[2])):
        groups[key] = sorted([version for version, *_ in group],
                             key=lambda version: parse(version))

    return [Requiree(*key, versions) for key, versions in groups.items()]


def release_page(package_name, version):
    release = session.query(Release).filter_by(version=version)\
        .join(Package).filter_by(name=package_name).one_or_none()

    if not release:
        # TODO: Real 404
        return f"404 - {package_name} version {version} not found"

    end = Pendulum.utcnow()
    ten_day_download_count = get_download_count(end - timedelta(days=10), end, release)

    template_data = {
        'package': release.package,
        'release': release,
        'requirements': get_requirements(release),
        'unresolved_requirements': get_unresolved_requirements(release),
        'requirees': get_requirees(release),
        'ten_day_download_count': ten_day_download_count
    }

    return render_template('release.html', **template_data)


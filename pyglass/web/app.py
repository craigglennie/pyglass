from flask import Flask

from .package import package_page
from .release import release_page
from .stats import stats_page
from os import path

template_dir = f'{path.split(__file__)[0]}/../../templates'
static_dir = f'{path.split(__file__)[0]}/../../static'
app = Flask('pyglass', template_folder=template_dir, static_folder=static_dir)

app.add_url_rule(
    '/package/<name>',
    view_func=package_page
)

app.add_url_rule(
    '/package/<package_name>/version/<version>',
    view_func=release_page
)

app.add_url_rule(
    '/stats',
    view_func=stats_page
)


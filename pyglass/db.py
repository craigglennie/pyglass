import os
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker

from sqlalchemy.orm import scoped_session

from pyglass import config

_engine = None
session = None


def get_or_create_session(connection_string=None):
    global session, _engine
    if not session:
        if not connection_string:
            connection_string = config.value("DB_CONNECTION_STRING")

        # Default pool_size (5) and max_overflow(10) causes problems when queueing
        # releases, where the ThreadPoolExecutor defaults to 5 x numCores
        _engine = create_engine(connection_string, pool_size=20, max_overflow=40)
        session_factory = sessionmaker(bind=_engine)
        session = scoped_session(session_factory)


get_or_create_session()


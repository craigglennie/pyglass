import json
from enum import IntEnum
from typing import List
from urllib.error import HTTPError

import attr
import boto3

from pyglass import config, dependencies, common
from pyglass.common import sqs_result_queue, sqs_task_queue
from pyglass.dependencies import RequirementSet, RequirementSetType, DownloadType, \
    FailedToFindRequirements, MissingReleaseData, MissingVersionData


logger = common.get_logger(__name__)


def requirements_json(package_name: str, version: str, requirements: List[RequirementSet],
                      download_type: DownloadType, **kwargs) -> dict:

    def serialize_requirement_set_type(item):
        if isinstance(item, RequirementSetType):
            return item.value
        raise TypeError(f"Can't JSON serialize {item}")

    data = dict(kwargs)
    data.update({
        'package_name': package_name,
        'version': version,
        'requirements': [attr.asdict(r) for r in requirements],
        'download_type': download_type.value
    })

    return json.dumps(data, default=serialize_requirement_set_type)


def s3_result_key(package_name: str, version: str) -> str:
    return f'{package_name}/{version}.json'


# Annoying convenience for mocking in testing
def _s3():
    return boto3.resource('s3', region_name='us-east-1')


def write_requirements_to_s3(package_name, version, requirements, download_type):
    s3 = _s3()
    bucket_name = config.value('S3_REQUIREMENTS_BUCKET_NAME')
    assert bucket_name, 'Bucket name must be specified in .env file'
    obj = s3.Object(
        bucket_name,
        s3_result_key(package_name, version)
    )
    obj.put(Body=(requirements_json(package_name, version, requirements, download_type)))


class WorkerResult(IntEnum):
    ERROR = 0
    SUCCESS = 1


def _sqs_error_result_msg(package_name: str, version: str,
                          error_reason: str, error_message: str) -> dict:
    return json.dumps({
        'result': WorkerResult.ERROR.value,
        'package_name': package_name,
        'version': version,
        'error_reason': error_reason,
        'error_message': error_message
    })


def queue_result_message(message: dict):
    queue = sqs_result_queue()
    try:
        queue.send_message(MessageBody=message)
    except Exception:
        logger.exception('Error queuing result message', exc_info=True)


def error_to_data(exception: Exception):
    """Returns a string reason and message for a caught exception"""
    reason, message = None, None
    if isinstance(exception, FailedToFindRequirements):
        reason = exception.result
        message = str(exception)
    elif isinstance(exception, MissingVersionData):
        reason = 'Missing version data'
    elif isinstance(exception, MissingReleaseData):
        reason = 'Missing release data'
    elif isinstance(exception, HTTPError):
        reason = f'HTTP {exception.code}'
        message = exception.reason
    else:
        reason = exception.__class__.__name__
        message = str(exception)
    return reason, message


def process_task_queue(run_forever: bool):
    task_queue = sqs_task_queue()
    while 1:
        batch = task_queue.receive_messages(MaxNumberOfMessages=10, WaitTimeSeconds=10)
        if not len(batch) and not run_forever:
            break
        for message in batch:
            try:
                body = json.loads(message.body)
                requirements_task(body['package_name'], body['version'])
            except Exception:
                logger.exception(f"Error processing message: '{message.body}'", exc_info=True)
            finally:
                message.delete()


def requirements_task(package_name: str, version: str):
    """task to be executed by a remote worker, fed from an SQS queue"""
    try:
        download_type, reqs = dependencies.get_version_requirements(package_name, version)
        write_requirements_to_s3(package_name, version, reqs, download_type)
        msg = requirements_json(package_name, version, reqs, download_type,
                                result=WorkerResult.SUCCESS.value)
    except Exception as err:
        reason, message = error_to_data(err)
        msg = _sqs_error_result_msg(package_name, version, reason, message)

    queue_result_message(msg)


if __name__ == '__main__':
    # Use run_forever because we rely on autoscaling to kill the machine when
    # the queue has stayed empty for some period
    process_task_queue(True)
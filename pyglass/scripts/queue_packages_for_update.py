"""Script to get the latest releases from PyPI's RSS feed
and queue them for updating"""

import feedparser
from pyglass.models import Release, QueuedUpdate, Package
from pyglass.db import session
from pyglass.common import get_logger


logger = get_logger(__name__)


def _get_rss_entries():
    results = feedparser.parse('http://pypi.python.org/pypi?%3Aaction=rss')
    return results['entries']


def run():
    logger.info("Reading RSS feed for package releases")
    for result_dict in _get_rss_entries():
        # example: http://pypi.python.org/pypi/graphmachine/3.2.3.105
        package_name = result_dict['link'].split("/")[4]
        version = result_dict['link'].split("/")[5]

        query = session.query(Release).filter(Release.version == version)
        query = query.join(Package).filter(Package.name == package_name)
        if query.count():
            continue
        if session.query(QueuedUpdate).filter_by(package_name=package_name).count():
            continue

        session.add(QueuedUpdate(package_name=package_name))
        logger.info(f"Queued for update: '{package_name}'")
        session.commit()





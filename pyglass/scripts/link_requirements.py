import json
from typing import List
from urllib.error import HTTPError
from concurrent.futures import ThreadPoolExecutor
from concurrent import futures

# from packaging.requirements import Requirement as PkgRequirement, InvalidRequirement
import pkg_resources

from pyglass.common import sqs_result_queue, get_logger, sqs_task_queue
from pyglass import dependencies
from pyglass.db import session
from pyglass.dependencies import DownloadType, FailedToFindRequirements, \
    RequirementSetType, MissingVersionData
from pyglass.models import Release, Package, QueuedUpdate, Requirement, \
    UnresolvedRequirement
from pyglass.scripts.requirements_worker import WorkerResult

logger = get_logger(__name__)


def get_release_ids_to_process(max_per_package: int, max_attempt_count: int,
                               only_new_packages: bool) -> List[int]:
    assert max_per_package > 0

    # Could use SQLAlchemy's Lateral Join support, but it doesn't seem any easier
    # than just writing the SQL out
    sql = ("SELECT distinct(r1.package_id), r2.* FROM releases r1 "
           "INNER JOIN LATERAL (SELECT id FROM releases "
           "WHERE releases.package_id = r1.package_id "
           f"AND releases.requirements_attempt_count <= {max_attempt_count} "
           f"AND releases.is_queued = false "
           f"ORDER BY releases.released_at DESC LIMIT {max_per_package}) "
           "r2 ON TRUE")

    if only_new_packages:
        sql += (" AND r1.package_id NOT IN "
                "(SELECT DISTINCT package_id FROM releases "
                "WHERE requirements_attempt_count > 0)")

    return [r_id for p_id, r_id in session.execute(sql)]


def _get_task_message(release: Release) -> dict:
    body = json.dumps({
        'package_name': release.package.name,
        'version': release.version
    })
    message = {
        'Id': str(release.id),
        'MessageBody': body
    }
    return message


def _queue_release_batch(release_ids: List[int]):
    releases = session.query(Release).filter(Release.id.in_(release_ids))
    for release in releases:
        release.is_queued = True
        release.requirements_attempt_count += 1
        session.add(release)
    session.commit()
    queue = sqs_task_queue()
    queue.send_messages(Entries=[_get_task_message(release) for release in releases])


def queue_releases(release_ids: List[int]) -> None:

    def chunks(l: List, n: int) -> List[List]:
        for i in range(0, len(l), n):
            yield l[i:i + n]

    with ThreadPoolExecutor() as executor:
        future_to_release_ids = {
            executor.submit(_queue_release_batch, chunk): chunk for chunk in chunks(release_ids, 10)
        }
        for future in futures.as_completed(future_to_release_ids):
            try:
                future.result()
            except Exception as exc:
                logger.exception("Error trying to queue releases", exc_info=exc)
                failed_release_ids = future_to_release_ids[future]
                session.query(Release).filter(Release.id.in_(failed_release_ids))\
                    .update({'is_queued': False}, synchronize_session='fetch')
                session.commit()


def get_or_create_requirement(release: Release, package: Package, req_type: str,
                              environment: str, specifier: str) -> Requirement:

    req = session.query(Requirement).filter_by(
        release=release, package=package, req_type=req_type, environment=environment
    ).one_or_none()
    if not req:
        req = Requirement(release=release, package=package, req_type=req_type,
                          environment=environment, specifier=specifier)
    return req


def get_or_create_unresolved_requirement(
        release: Release, queued_update: QueuedUpdate,
        req_type: str, environment: str, specifier: str) -> UnresolvedRequirement:

    req = session.query(UnresolvedRequirement).filter_by(
        release=release, queued_update=queued_update, req_type=req_type, environment=environment
    ).one_or_none()
    if not req:
        req = UnresolvedRequirement(queued_update=queued_update, specifier=specifier,
                                    req_type=req_type, environment=environment, release=release)
    return req


def link_requirement(release: Release, package_name: str, specifier,
                     req_type: RequirementSetType, environment: str,
                     from_wheel: bool):
    """Create Requirement and/or UnresolvedRequirement rows for 'release'.
    If there's not a QueuedUpdate already for any packages in
    UnresolvedRequirement then create one"""
    package = session.query(Package).filter_by(name=package_name).first()
    if package:
        req = get_or_create_requirement(release, package, req_type.value, environment, specifier)
    else:
        queued = session.query(QueuedUpdate).filter_by(package_name=package_name).first()
        if not queued:
            queued = QueuedUpdate(package_name=package_name)
            session.add(queued)
            session.commit()
        req = get_or_create_unresolved_requirement(
            release, queued, req_type.value, environment, specifier
        )
    req.from_wheel = from_wheel
    session.add(req)

    session.commit()


def process_sqs_message(message: dict):
    """Reads a result message from SQS and links the relevant release, handling any errors"""

    query = session.query(Release).filter_by(version=message['version'])
    release = query.join(Package).filter_by(name=message['package_name']).one()

    if message['result'] == WorkerResult.SUCCESS:
        error_parsing_requirements = False
        reqs = message['requirements']
        for req_set in reqs:
            for req_str in req_set['requirements']:
                # Don't assume requirements are well formatted. Looking at you, boto3 v1.2.0
                try:
                    requirement = parse_req(req_str)
                except Exception:
                    error_parsing_requirements = True
                    logger.info(f"Failed to parse requirements string '{req_str}' "
                                f"for {message['package_name']} {message['version']}",
                                exc_info=True)
                    continue
                link_requirement(
                    release, requirement.name, str(requirement.specifier),
                    RequirementSetType(req_set['set_type']), req_set['environment'],
                    message['download_type'] == DownloadType.WHEEL
                )
        release.error_parsing_requirements = error_parsing_requirements
    else:
        release.requirements_error_message = message['error_message']
        release.requirements_error_reason = message['error_reason']

    release.is_queued = False
    session.add(release)
    session.commit()


def process_result_queue():
    queue = sqs_result_queue()
    count = 0
    while 1:
        batch = queue.receive_messages(MaxNumberOfMessages=10, WaitTimeSeconds=10)
        if not len(batch):
            break
        for message in batch:
            process_sqs_message(json.loads(message.body))
            message.delete()
            count += 1
        if not count % 100:
            logger.info(f'Processed {count} SQS messages')


def parse_req(req_str: str):
    reqs = list(pkg_resources.parse_requirements(req_str))
    if len(reqs) > 1:
        raise AssertionError(f'Multiple requirements in "{req_str}": {reqs}')
    return reqs[0]


def process_release(release: Release):

    try:
        download_type, reqs = dependencies.get_version_requirements(
            release.package.name, release.version)
        for req_set in reqs:
            for requirement in [parse_req(req_str) for req_str in req_set.requirements]:
                link_requirement(
                    release, requirement.name, str(requirement.specifier),
                    req_set.set_type, req_set.environment,
                    download_type == DownloadType.WHEEL
                )

    except FailedToFindRequirements as error:
        release.requirements_error_reason = error.result
        release.requirements_error_message = str(error)
        logger.info("Permanent failure getting requirements for %s", release)
    # TODO: Possibly retry some HTTP Errors, depending on code?
    except HTTPError as error:
        release.requirements_error_reason = f'HTTP {error.code}'
        release.requirements_error_message = error.reason
        logger.info("HTTP Error getting package data for %s %s", release,
                     error)
    # TODO: Possibly retry these too
    except MissingVersionData:
        release.requirements_error_reason = f'Missing version data'
        logger.info("Didn't find version data for %s", release)

    release.requirements_attempt_count += 1
    session.add(release)
    session.commit()


# Process locally
def link_all(max_per_package: int=5, max_attempt_count: int=0, only_new_packages: bool=True):
    for release_id in get_release_ids_to_process(max_per_package, max_attempt_count, only_new_packages):
        try:
            release = session.query(Release).filter_by(id=release_id).one()
            process_release(release)
        except Exception as err:
            release.requirements_error_reason = f'Exception: {err.__class__.__name__}'
            release.requirements_error_message = str(err)
            release.requirements_attempt_count += 1
            session.add(release)
            session.commit()
            logger.warning(f"Exception caught in generic handler for {release}")
            logger.warning(err)
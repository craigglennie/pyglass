"""Fetch aggregated download data from BigQuery, and load it into
our local database. Any unknown packages, or packages with previously-unseen
versions, are queued for update from Pypi"""

from collections import namedtuple
from tempfile import NamedTemporaryFile
import csv
import os
from typing import List
import multiprocessing
import logging
from sqlalchemy import update, delete, insert
from sqlalchemy.orm import aliased

import pendulum
import postgres_copy
from pyglass.db import _engine, session
from google.cloud import bigquery
from pyglass.models import SuccessfulBigQueryExtraction, Package, Release, \
    QueuedUpdate, Download, StagedDownload

logging.basicConfig(level=logging.INFO,
                    format="%(asctime)s %(levelname)s"
                           " %(module)s.%(funcName)s:%(lineno)d - %(message)s")
logger = logging.getLogger(__name__)


def _log_time(func, *args, msg_prefix=None, **kwargs):
    started_at = pendulum.datetime.utcnow()
    logger.info(f'{msg_prefix} beginning')
    result = func(*args, **kwargs)
    elapsed = (pendulum.datetime.utcnow() - started_at).as_interval()
    logger.info(f"{msg_prefix} completed in {str(elapsed)}")
    return result


def _get_table_names_to_query(client: bigquery.Client) -> List[str]:
    """Get a list of download data table names from the Pypi BigQuery dataset
    
    There are some other tables in the dataset that don't contain download data,
    we want to ignore those. We also ignore tables we have already processed"""
    pypi_dataset = client.dataset('pypi', 'the-psf')
    tables = sorted(pypi_dataset.list_tables(), key=lambda table: table.name)
    # The last table in this list is for the current day, and is still
    # having data streamed into it from Pypi
    all_bigquery_tables = [table.name for table in tables[:-1]]
    existing = {obj.table_name for obj in session.query(SuccessfulBigQueryExtraction)}
    # Exclude the 'downloads' table, since it doesn't get updated. Useable
    # data is only in the daily tables
    return [name for name in all_bigquery_tables if name not in existing and name != 'downloads']


def _get_bigquery_sql(table_name: str) -> str:
    """Returns the BigQuery-compatible SQL used to query a downloads table"""
    # SQL string functions are broken out testing purposes
    return ("SELECT file.project, file.version, details.python, COUNT(*) FROM "
            f"[the-psf:pypi.{table_name}] GROUP BY file.project, file.version, "
            "details.python ORDER BY file.project, file.version")


def _get_rows_from_bigquery(client: bigquery.Client, table_name: str) -> List[List[str]]:
    """Returns aggregated data from BigQuery, handling pagination
    automatically"""
    sql_string = _get_bigquery_sql(table_name)
    query = client.run_sync_query(sql_string)
    logger.info(f'BigQuery: beginning query on table {table_name}')
    _log_time(query.run, msg_prefix=f'BigQuery: query on table {table_name}')

    rows = []

    def paginate(token):
        downloads, total_rows, token = query.fetch_data(100000, token)
        rows.extend(downloads)
        if not total_rows:
            logger.warning(f'{table_name} got {total_rows} for total rows. {len(downloads)} rows in page')
        else:
            pct_complete = (len(rows) / total_rows) * 100
            logger.info("BigQuery: rows fetched %d of %d (%.1f)%%",
                        len(rows), total_rows, pct_complete)
        if token:
            paginate(token)

    _log_time(paginate, None,
              msg_prefix=f'Bigquery: result fetch for table {table_name}')
    return rows


StagingTableRow = namedtuple(
    'StagingTableRow',
    ['for_date', 'created_at', 'package_name', 'package_version',
     'python_version', 'downloads']
)


def _copy_into_staging(rows: List[StagingTableRow]):
    """Use Postgres' COPY FROM syntax to write `rows` to a text file and load
    them into the Staging table"""
    temp_dir = os.environ['PG_COPY_FROM_HOST_PATH']
    with NamedTemporaryFile('w', dir=temp_dir) as tempfile:
        writer = csv.writer(tempfile, delimiter='\t')
        writer.writerows(rows)
        tempfile.flush()

        with open(tempfile.name) as infile:
            _log_time(postgres_copy.copy_from, infile,
                      StagedDownload.__table__, _engine,
                      columns=StagingTableRow._fields,
                      msg_prefix=f'Copy {len(rows)} rows into staging table')


def _load_table(downloads: List[List[str]], for_date: str):

    created_at = str(pendulum.utcnow())

    def table_row(package_name, package_version, python_version, downloads):
        return StagingTableRow(
            for_date,
            created_at,
            package_name,
            package_version,
            # Replace empty string with the default NULL indicator
            python_version or '\\N',
            downloads
        )

    db_rows = [table_row(*download) for download in downloads]
    _copy_into_staging(db_rows)


def _queue_packages_for_update():
    """Creates entries in the PypiUpdateQueue table for packages that aren't
     already queued"""
    query = lambda model: session.query(model.package_name).distinct()
    staged_download_names = {name for [name] in query(StagedDownload)}
    queued_update_names = {name for [name] in query(QueuedUpdate)}

    to_add = staged_download_names.difference(queued_update_names)
    for name in to_add:
        session.add(QueuedUpdate(package_name=name))
    session.commit()
    logger.info(f"Queued {len(to_add)} packages for update")


def _date_from_table_name(table_name) -> str:
    """Returns a string representing that date that a download table
    contains data for"""
    date = pendulum.strptime(table_name, "downloads%Y%m%d")
    return date.strftime("%Y-%m-%d")


def _copy_staging_to_downloads():
    """Updates the Staging table with data from Package and Release, then
    copies it into the Download table"""

    set_package_id = update(StagedDownload).values(
        package_id=Package.id).where(
        StagedDownload.package_id == None).where(
        StagedDownload.package_name == Package.name)
    session.execute(set_package_id)

    set_release_id = update(StagedDownload).values(
        release_id=Release.id).where(
        StagedDownload.package_id != None).where(
        StagedDownload.release_id == None).where(
        StagedDownload.package_id == Release.package_id).where(
        StagedDownload.package_version == Release.version)
    session.execute(set_release_id)


    copy_cols = ['package_id', 'release_id', 'python_version', 'for_date',
                 'downloads']
    get_table_cols = lambda table: [getattr(table, col) for col in copy_cols]

    copy_query = session.query(*get_table_cols(StagedDownload)).filter(
        StagedDownload.package_id != None,
        StagedDownload.release_id != None
    )
    copy_to_downloads = insert(Download).from_select(get_table_cols(Download), copy_query)
    session.execute(copy_to_downloads)

    # Complicated join to make sure that we only delete rows from
    # StagedDownload where a Download exists with the same package_id
    # and release_id
    download_alias = aliased(Download)
    sub_select = session.query(StagedDownload.id).join(
        Package, StagedDownload.package_id == Package.id).join(
        Release, Release.id == StagedDownload.release_id).join(
        Download, Download.package_id == Package.id).join(
        download_alias, download_alias.release_id == Release.id)

    cleanup_staging = session.query(StagedDownload).filter(
        StagedDownload.id.in_(sub_select))

    cleanup_staging.delete(False)


def run():
    """Loads new data from BigQuery into the database, and queues any unseen
    packages or versions for updating from Pypi.
    
    There is unfortunately lot of raw SQL string building here, because I
    couldn't see how to easily do this kind of thing with Django's ORM.
    This is where I miss SQLAlchemy :)"""

    # Connection requires a GOOGLE_APPLICATION_CREDENTIALS environment
    # variable to be pointing to a Service Account JSON file. Should
    # be set in the .env file for the project
    client = bigquery.Client()

    table_names = _get_table_names_to_query(client)
    for table_name in table_names:
        downloads = _get_rows_from_bigquery(client, table_name)
        date_string = _date_from_table_name(table_name)
        _load_table(downloads, date_string)
        session.add(SuccessfulBigQueryExtraction(table_name=table_name))
        session.commit()

    _copy_staging_to_downloads()
    _queue_packages_for_update()


def _download_to_disk(args):
    """Helper function for splitting downloads across multiple processes"""
    table_name, directory = args
    client = bigquery.Client()
    rows = _get_rows_from_bigquery(client, table_name)
    with open(f"{directory}/{table_name}.csv", "w") as outfile:
        csv.writer(outfile).writerows(rows)


def batch_download_to_disk(start_date, end_date, directory, skip_existing=True, processes=3):
    """Download data between a given date range to the target directory,
    using multiple processes.
    
    This is useful for getting data for testing, for pre-population, or for
    backfilling missing data"""
    current_date = start_date
    table_names = []
    while current_date <= end_date:
        table_name = f"downloads{current_date.strftime('%Y%m%d')}"
        if skip_existing and os.path.exists(os.path.join(directory, table_name + '.csv')):
            logger.info("Skipping existing table %s", table_name)
        else:
            table_names.append(table_name)
        current_date = current_date.add(days=1)
    pool = multiprocessing.Pool(processes=processes)
    pool.map(_download_to_disk, ((table_name, directory) for table_name in table_names))
    pool.close()
    pool.join()


def load_download_from_disk(downloads_file_path: str):
    with open(downloads_file_path) as infile:
        rows = list(csv.reader(infile))
    table_name = os.path.basename(downloads_file_path).replace(".csv", "")
    date_str = _date_from_table_name(table_name)
    _load_table(rows, date_str)
    session.add(SuccessfulBigQueryExtraction(table_name=table_name))
    session.commit()



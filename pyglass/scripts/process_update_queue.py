"""Fetches package update information from pypi.python.org,
which publishes and RSS feed of the 40 most recent updates."""

import json
import logging
from typing import List
from urllib.error import HTTPError, URLError
from sqlalchemy import or_

import pendulum
from packaging.version import parse

from pyglass import config
from pyglass.common import get_pypi_json
from pyglass.db import session
from pyglass.models import Release, Package, QueuedUpdate, Requirement, \
    UnresolvedRequirement
from pyglass.scripts.link_requirements import get_or_create_requirement

logger = logging.getLogger(__name__)


def _create_or_update_package(package_name: str, author: str, home_page: str,
                              summary: str) -> Package:
    package = session.query(Package).filter_by(name=package_name).first()
    if not package:
        package = Package(name=package_name)
    if package.author != author or package.home_page != home_page:
        package.author = author
        package.home_page = home_page
        package.summary = summary
    session.add(package)
    session.commit()
    return package


def _order_releases(package: Package, new_releases: List[Release]=None):
    """Use the 'packaging' package to order releases, since
    it knows how to parse and compare version strings. new_releases
    may not have been committed to the database yet"""
    releases = session.query(Release).filter_by(package=package).all()
    releases.extend(new_releases or [])
    releases.sort(key=lambda obj: parse(obj.version))
    for i, release in enumerate(releases):
        release.ordering = i
        session.add(release)
    session.commit()


def _get_packages_to_update() -> List[QueuedUpdate]:
    query = session.query(QueuedUpdate).filter(
        or_(QueuedUpdate.retry_after == None, QueuedUpdate.retry_after < pendulum.utcnow())
    )
    return query.all()


def _queued_update_failed(queued_update: QueuedUpdate):
    max_backoff = int(config.value("UPDATE_QUEUE_MAX_BACKOFF_FACTOR"))
    if queued_update.back_off_factor >= max_backoff:
        logger.warning(f"Max back-off reached for "
                       f"'{queued_update.package_name}', giving up")
        session.delete(queued_update)
    else:
        queued_update.back_off_factor += 1
        queued_update.retry_after = \
            pendulum.utcnow().add(minutes=2 ** queued_update.back_off_factor)
        session.add(queued_update)
        session.commit()


def load_json(data: dict) -> bool:

        release_data = data.get('releases')
        if not release_data:
            return None

        package_info = data.get('info')
        if not package_info:
            return None

        author = package_info.get("author")
        home_page = package_info.get("home_page")
        summary = package_info.get("summary")
        package = None

        missing_version_data = False
        # pypi JSON data contains all versions, so if we happened to miss one
        # we'll catch up here
        releases_to_save = []
        for version, version_info in release_data.items():
            # Sometimes version info is missing, in which case we haven't successfully
            # updated all package info. Take what we can, but also back off
            #  and try again
            if not version_info:
                missing_version_data = True
                continue
            # Do this here so that we don't create packages if we can't get version info
            if not package:
                package = _create_or_update_package(package_info["name"], author, home_page, summary)
            query = session.query(Release).filter_by(package=package).filter_by(version=version)
            if not query.count():
                # This is actually a list of releases by platform / OS, I think
                # see release 0.9.3a0 of mxnet at https://pypi.python.org/pypi/mxnet/json
                release_times = sorted(pendulum.parse(version['upload_time']) for version in version_info)
                release = Release(package=package, version=version, released_at=release_times[0])
                releases_to_save.append(release)

        if releases_to_save:
            _order_releases(package, new_releases=releases_to_save)

        return None if missing_version_data else package


def resolve_requirements(unresolved_reqs: List[UnresolvedRequirement],
                         package: Package):
    for unresolved in unresolved_reqs:
        req = get_or_create_requirement(
            unresolved.release,
            package,
            unresolved.req_type,
            unresolved.environment,
            unresolved.specifier
        )
        # req = Requirement(release_id=unresolved.release_id, package=package,
        #                   specifier=unresolved.specifier,
        #                   from_wheel=unresolved.from_wheel,
        #                   environment=unresolved.environment,
        #                   req_type=unresolved.req_type)
        req.from_wheel = unresolved.from_wheel
        session.add(req)
        session.commit()
        session.delete(unresolved)
    logger.info(f"Resolved {len(unresolved_reqs)} requirements for package '{package.name}'")


def run():
    """Process all queued updates"""
    for queued_update in _get_packages_to_update():
        logger.info(f"Processing queued update for '{queued_update.package_name}' "
                    f"Back-off factor: {queued_update.back_off_factor}")
        try:
            data = get_pypi_json(queued_update.package_name)
        # TODO: Catch all exceptions?
        except (HTTPError, URLError) as e:
            logger.info('Error fetching Pypi data for %s', queued_update.package_name, exc_info=e)
            _queued_update_failed(queued_update)
            continue

        try:
            package = load_json(data)
            if package:
                unresolved = session.query(UnresolvedRequirement).filter_by(
                    queued_update=queued_update).all()
                resolve_requirements(unresolved, package)
                session.delete(queued_update)
                session.commit()
            else:
                _queued_update_failed(queued_update)
        # TODO: Log exceptions, don't silently swallow them
        except Exception as e:
            logger.exception(str(e), exc_info=e)
            _queued_update_failed(queued_update)



def load_package_from_disk(package_file: str):
    """Helper function to load package data from a file"""
    with open(package_file) as infile:
        data = json.load(infile)
    load_json(data)

"""Replaces distutils.core, providing all the same features,
except for overriding the setup function to write JSON of the passed
arguments"""

import json
import os

from distutils.old_core import *

def setup(**kwargs):
    # Extensions (ext_modules) are C code utlitised by the package, which
    # needs to be compiled. The Extension class object is
    # not JSON serializable, and we don't have any use for it
    # cmdclass is custom config for building the package, also
    # not interesting.
    # long_description is the detailed description of the package,
    # and we're not Pypi.
    discard_keys = ['long_description', 'ext_modules', 'cmdclass']
    to_discard = [key for key in discard_keys if key in kwargs]
    for key in to_discard:
        kwargs.pop(key)

    env_var = 'PYGLASS_JSON_PATH'
    assert env_var in os.environ, \
        ("You must specify %s in the environment so this module knows where"
         " to write setup JSON to") % env_var
    with open("%s/dependencies.json" % os.environ[env_var], "w") as outfile:
        json.dump(kwargs, outfile)

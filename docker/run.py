"""Run the installer (really our replacement for it)
in the directory provided as an argument when the container
was started."""

# NOTE: If changes made here don't seem to be working make
# sure to rebuild the container!

import sys
import os
import traceback
from functools import partial

# A backport of Python 3's subprocess updates
if os.name == 'posix' and sys.version_info[0] < 3:
    import subprocess32 as subprocess
else:
    import subprocess


def write_result(package_dir, filename, content, return_status):
    with open("%s/%s" % (package_dir, filename), "w") as outfile:
        outfile.write(str(content))
    sys.stdout.write(return_status)

if __name__ == '__main__':
    assert len(sys.argv) == 3, ("Missing required arguments: directory "
                                "containing package source, and timeout "
                                "in seconds. Eg, python run.py /package 120")
    try:
        package_dir = sys.argv[1]
        write = partial(write_result, package_dir)
        source_path = '%s/source.tar.gz' % package_dir
        assert os.path.exists(source_path), "No such file %s" % source_path

        # Uncompress the source code, stripping the directory name
        # and unpacking it's contents directly into package_dir
        args = ['tar', '--strip-components=1', '-zxf', source_path]
        subprocess.check_output(args, cwd=package_dir)

        timeout = int(sys.argv[2])
        args = ['python', 'setup.py', 'install']
        env = dict(os.environ)
        env['PYGLASS_JSON_PATH'] = package_dir
        subprocess.check_output(
            args,
            stderr=subprocess.STDOUT,
            cwd=package_dir,
            timeout=timeout,
            env=env
        )
        sys.stdout.write('success')
    except subprocess.CalledProcessError as err:
        write('setup-error.output', err.output, 'setup-error')
    except subprocess.TimeoutExpired as err:
        write('timeout.output', '', 'timeout')
    except Exception as err:
        message = "".join(traceback.format_exception(*sys.exc_info()))
        write('exception.output', message, 'exception')
    finally:
        sys.stdout.flush()

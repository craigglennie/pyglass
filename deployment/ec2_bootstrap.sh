#!/bin/bash
set -x

# This script should be executed when an instance starts up.
# It copies and runs a setup script from S3 based on the
# environment tag of the instance.

# grep -v user-data otherwise the instance-id grep matches this script
instance_id=`ec2metadata | grep -v user-data | grep instance-id | cut -d' ' -f2`
echo EC2_INSTANCE_ID="$instance_id" >> /etc/environment
apt-get -y install awscli

# Hacky way to set the pyglass environment variable. Tags don't seem to be instantly available,
# loop for a bit until we find them
# TODO: Send an alert if we fail to find the tags, so the machine isn't up and idle
max_sleep="300"
sleep_interval="5"
slept_for="0"
while [ $slept_for -lt $max_sleep ]
do
        aws ec2 describe-tags --region us-east-1 --filters "Name=resource-id,Values=$instance_id" > /tags.json
        if grep environment /tags.json
        then
                break
        fi
        sleep $sleep_interval
        slept_for=$[$slept_for+$sleep_interval]
done

# Pull it out of the tags JSON
py_command="import json; data = json.load(open('/tags.json')); key = [i for i in data['Tags'] if i['Key'] == 'environment']; print(key[0]['Value'])"
environment=`python3 -c "$py_command"`
echo PYGLASS_ENV="$environment" >> /etc/environment

# Copy actual startup file from S3 bucket determined by environment
startup_file_name=requirements_worker_startup.sh
aws s3 cp s3://pyglass-"$environment"-resources/"$startup_file_name" .
chmod +x "$startup_file_name"
echo "Bootstrap complete"
./"$startup_file_name"

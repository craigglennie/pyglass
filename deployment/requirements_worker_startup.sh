#!/bin/bash
set -x

echo "Setting up Pyglass Celery worker"
echo "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQDn2QGhh3lVs2qm95FBYu3mUTwJLN7oMRk9ivKhc9hL4vTGV5xgAdpUGqI8WW5ixXg834gM+txCgmqv6wS0S+f53ewJgMdTDwXHd9UIS9X9vBMWvUP1WgMQKoGhK8ufKgCCyijhXAyblrXEpMwXb3+b2mcshZrVGTVhCPZiUlRiV05TSLlJZ7hTROPJF6qJR27YCBvQ4T5emeQ9NjlIXSrLpxpwl8hjCQ6eHPwhw2hBQ+XvpmcjzXn8/iNGguvaNr32HYeEgumiPUkZwlIkbiQgcEyTR3dhlQE8YqxVUdC9NuAZve6CrQPnqjUezjOTYUdAuV0K17yac+QqsUhKin0v craigglennie@Craigs-Air" >> ~ubuntu/.ssh/authorized_keys

# Fix annoying 'can't resolve hostname' error from sudo
echo "127.0.0.1 $(hostname)" >> /etc/hosts

# Add random apt PPA for Python 3.6
# per http://ubuntuhandbook.org/index.php/2017/07/install-python-3-6-1-in-ubuntu-16-04-lts/
add-apt-repository -y ppa:jonathonf/python-3.6

# Install Docker per https://www.digitalocean.com/community/tutorials/how-to-install-and-use-docker-on-ubuntu-16-04
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -
add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu $(lsb_release -cs) stable"
apt-get -y update
apt-cache policy docker-ce

apt-get -y install htop docker-ce python3.6 python3-pip git
# Required for pycurl to install; pycurl seems to be required by celery
apt-get -y install libcurl4-openssl-dev python3.6-dev libssl-dev

# Create logging directory
mkdir /var/log/pyglass
chown ubuntu:ubuntu /var/log/pyglass

# Install and run Cloudwatch Logs agent
curl https://s3.amazonaws.com//aws-cloudwatch/downloads/latest/awslogs-agent-setup.py -O
chmod +x ./awslogs-agent-setup.py
./awslogs-agent-setup.py -n -r us-east-1 -c s3://pyglass-dev-resources/cloudwatch_logs.config

# Setup virtualenv and app
pip3 install virtualenv
cd ~ubuntu
sudo -u ubuntu -H bash -c 'virtualenv --python=python3.6 venv-pyglass'
sudo -u ubuntu -H bash -c 'git clone https://craigglennie@bitbucket.org/craigglennie/pyglass.git pyglass'
sudo -u ubuntu -H bash -c 'aws s3 --region us-east-1 cp s3://pyglass-dev-resources/.env pyglass/.env'
sudo -u ubuntu -H bash -c 'source venv-pyglass/bin/activate && pip3 install -r pyglass/requirements.txt'

# Let the ubuntu user use docker
usermod -aG docker ubuntu

service docker restart
sudo -u ubuntu -H bash -c 'source venv-pyglass/bin/activate && cd pyglass/docker && make containers'
sudo -u ubuntu -H bash -c 'source venv-pyglass/bin/activate && cd pyglass && python -m pyglass.scripts.requirements_worker'

